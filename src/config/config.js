export default {
  api: "https://api.bitdtc.com",
  config: {
    headers: {
      'Content-Type': 'application/json'
    }
  },
  dreamTech:{
    url:"https://demo.bitdtc.com"
  }
}