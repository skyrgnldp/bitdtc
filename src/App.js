import React, {Component} from 'react';
import {Router, Route, Switch} from 'react-router-dom';
import history from './js/history';
import HomeIndex from "./component/homePage/homeIndex";
import NewsDetail from "./component/news/newsDetails";
import RelatedNews from "./component/news/relatedNews";
import message from "antd/lib/message";
import AccountCenter from "./component/buytokens/accountCenter";
import WaitForVerify from "./component/loginRegister/waitForVerifyEmail";
import EmailConfirmed from "./component/loginRegister/emailConfirmed";
import SecuritySettings from "./component/accountCenter/securitySettings";
import PersonalInformation from "./component/accountCenter/personalInformation";
import WalletCenter from "./component/buytokens/walletCenter";
import AboutIndex from "./component/aboutPage/aboutIndex";
import Faqs from "./component/aboutPage/faqs";
import pageNotFound from "./component/errorHandlers/pageNotFound";
import LoginRegister from "./component/loginRegister/loginRegisterComponent";
import ForGetPassword from "./component/loginRegister/forgetpassword";

import 'antd/dist/antd.css';
message.config({
  top: 50,
  duration: 2,
  maxCount: 1,
});

class App extends Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <Route path="/" component={HomeIndex} exact/>
          <Route path="/loginregister/:type" component={LoginRegister}/>
          <Route path="/newsdetail/:id" component={NewsDetail}/>
          <Route path="/forgetpassword" component={ForGetPassword}/>
          <Route path="/relatedNews/:tag" component={RelatedNews}/>
          <Route path="/accountCenter" component={AccountCenter}/>
          <Route path="/verify" component={WaitForVerify}/>
          <Route path="/confirmed/:confirm" component={EmailConfirmed}/>
          <Route path="/security" component={SecuritySettings}/>
          <Route path="/walletCenter" component={WalletCenter}/>
          <Route path="/personal" component={PersonalInformation}/>
          <Route path="/aboutUs" component={AboutIndex}/>
          <Route path="/faqs" component={Faqs}/>
          <Route path='/404' exact={true} component={pageNotFound} />
          <Route component={pageNotFound} />
        </Switch>
      </Router>
    );
  }
}

export default App;
