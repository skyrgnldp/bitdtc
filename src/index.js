import React from 'react';
import ReactDOM from 'react-dom';
import './css/common.css';
import './css/animate.css';

import App from './App';
import registerServiceWorker from './js/registerServiceWorker';

ReactDOM.render(<App/>, document.getElementById('root'));
registerServiceWorker();
