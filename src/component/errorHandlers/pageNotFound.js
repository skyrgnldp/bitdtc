import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import '../../css/errorHandlers/pageNotFound.css';

class pageNotFound extends Component {
  render(){
    return(
      <div className='error404'>
        <span>
          <img src={require("../../images/error404-01.svg")} id="svgMain" alt="error404" />
          <Link to="/" className="returnLink">RETURN</Link>
        </span>
      </div>
    );
  }
}
export default pageNotFound;
