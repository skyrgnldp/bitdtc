import React, {Component} from 'react';
import {Modal} from 'antd';
import "../../css/popupwindow/popupwindow.css";

class PopupWindow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  };
  componentDidMount(){
    this.props.onRef(this)
  }
  showModal = () => {
    this.setState({
      visible: true
    });
  };
  handleCancel = (e) => {
    this.setState({
      visible: false
    });
  };
  render() {
    return (
      <div className="popupWindow">
        <Modal visible={this.state.visible}
               footer={null}
               maskClosable={false}
               onCancel={this.handleCancel}>
          <div className="popupWindowContent">
            <img src={require("../../images/logoSample2-01.svg")} alt="logo"/>
            <div className="userInformation">
              <h3>Thank you <br/>for your message !</h3>
              <h4>We got your message and will get back to <br/> you soon as we can.</h4>
            </div>
            <a>Follow us on:</a>
            <div className="displayflexcenter">
              <img src={require("../../images/followuson/Google+.svg")} alt="facebook"/>
              <img src={require("../../images/followuson/facebook.svg")} alt="facebook"/>
              <img src={require("../../images/followuson/instagram.svg")} alt="facebook"/>
              <img src={require("../../images/followuson/twitter.svg")} alt="facebook"/>
            </div>
            <h2> </h2>
            <p>© 2018 DreamTech. All rights reserved.</p>
          </div>
        </Modal>
      </div>
    );
  }
}

export default PopupWindow;
