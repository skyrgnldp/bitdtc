import React, {Component} from 'react';
import SubComponentHeader from '../subComponentHeader';
import SubComponentFooter from '../subComponentFooter';
import AboutShowcase from './aboutShowcase';

class AboutIndex extends Component {
  render(){
    return(
      <div className='about'>
        <SubComponentHeader/>
          <AboutShowcase />
        <SubComponentFooter/>
      </div>
    );
  }
}
export default AboutIndex;
