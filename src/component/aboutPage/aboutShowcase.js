import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import '../../css/aboutPage/aboutShowcase.css';

class AboutShowcase extends Component {
  render() {
    return (
      <div className="aboutShowcase">
        <div className="container">
          <p className="mainTitle">About us</p>
          <p className="mainSubtitle">DreamTech Cash Cryptocurrency</p>
          <img src={require("../../images/AboutUsBanner.svg")} alt="" className="aboutUsBanner"/>
          <div className="aboutUsInfos">
            <p>DreamTech aims to build a foundation for a respectable platform that has prominent
              security and advanced technologies. We hope that this ambitious project will serve as
              a basis for further development of alike technologies and bring innovation to the benefit
              of the public.</p>
            <p>We have been in the industry for a significant amount of time and has proven our skills
              through collaborating with various professionals and businesses. Our determined efforts,
              expertise, and compelling vision have resulted to the creation of the DreamTech network.
              We aim to integrate our knowledge and experience regarding brick-and-mortar and online
              casino technologies to this project. </p>
            <p>DreamTech is an interactive entertainment network that serves as a platform especially
              made for gamblers and cryptocurrency enthusiasts. Its features allow P2P connectivity
              among users thus allowing anonymity and security yet maintains the essence of
              entertainment in the network. It will also implement ranking and attendance rewards for
              more user engagement.</p>
            <p>The core goals of the DreamTech Network are:</p>
            <ol>
              <li>To provide a scalable and accessible online casino platform</li>
              <li>To provide service with lower and faster transaction costs</li>
              <li>To provide comfort, convenience, and entertainment</li>
              <li>To provide a secured platform to players and game operators</li>
            </ol>
            <p>The DreamTech platform ensures fairness in number generation, cards distribution,
              pay outs, and distribution of funds. The platform is designed to provide a secure
              network to those gambling and cryptocurrency enthusiasts </p>
          </div>
        </div>
        <hr/>
        <div className="questions">
          <b className="questionHeader">Questions?</b>
          <p>Refer to our <Link to="/faqs" className="faqLink">FAQs</Link> to answer any queries regarding DreamTech.
            You can also email us on <a
              href="https://mail.google.com/mail/?view=cm&fs=1&tf=1&to=support@dreamtech.org&su=HelloDreamTech&shva=1">support@dreamtech.org</a>
          </p>
          <p>or message us through our social media accounts - Telegram and Discord, for immediate support.</p>
        </div>
        <div className="bottomPart">
          <p className="title">Want to know how DreamTech Cash works?</p>
          <p className="subTitle">This crypto-powered decentralized network isn't as complicated as it sounds. How?
            Click the link to find out more:</p>
          <Link to="/404" className="clickHere">Click Here</Link>
        </div>
      </div>
    );
  }
}

export default AboutShowcase;
