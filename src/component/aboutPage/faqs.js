import React, {Component} from 'react';
import '../../css/aboutPage/faqs.css';
import SubComponentHeader from '../subComponentHeader';
import SubComponentFooter from '../subComponentFooter';
import {Link} from 'react-router-dom';

class Faqs extends Component {

  render(){
    return(
      <div className="faq">
        <SubComponentHeader />
          <div className="container">
            <p className="mainTitle">FAQ's</p>
            <p className="mainSubTitle">DreamTech Cash Cryptocurrency</p>
            <img src={require("../../images/Banner.svg")} alt="" className="showcaseImg" />
            <div className="aboutInfo">
              <b>What is DreamTech Cash?</b>
              <p>DreamTech is an interactive entertainment network that serves as a platform
                especially made for gamblers and cryptocurrency enthusiasts.  Its
                features allow P2P connectivity among users thus allowing anonymity and security yet
                maintains the essence of entertainment in the network.</p>
              <b>How is DreamTech different from other online casino providers?</b>
              <p>It is more efficient because it incorporates cryptocurrency and blockchain technology.</p>
              <b>How does DreamTech work?</b>
              <p>Any player who wants to navigate and experience the platform must first register his digital
                 identity to the network. The player can then enjoy features and its games.
                 Each game implements random number generators on their algorithm. The generated random numbers
                 assure the fairness amongst the games and services in the platform.</p>
              <b>Who can access DreamTech?</b>
              <p>Players must be of legal age (18) and above before he can access the network. Also, the casino
                platform must be legal to his current location of access. The player has accepted the Terms and
                Conditions in the process of registration. The network and the team will not be liable for any unfavorable
                actions regarding this issue.</p>
              <b>How am i sure that my connection is truly secured?</b>
              <p>The DreamTech is built over the blockchain architecture. All transactions passing through the network
                is fully encrypted. The decentralized network will not store any personal and confidential data.</p>
              <b>Payments</b>
              <p>All transactions must be used with the DTC token that is available during token sale. Refer to the
                DreamTech White paper for further details about the token sale.</p>
              <b>Payouts</b>
              <p>The payouts will be automatically and immediately credited to the player’s account. </p>
              <b>Anonymity</b>
              <p>The network ensures that there is no centralized server that will store the player’s personal
                data. Only the transactions are mined and therefore placed in the blockchain.</p>
            </div>
          </div>
          <div className="bottomPart">
            <p className="title">Want to know how DreamTech Cash works?</p>
            <p className="subTitle">This crypto-powered decentralized network isn't as complicated as it sounds. How? Click the link to find out more:</p>
            <Link to="/404" className="clickHere">Click Here</Link>
          </div>
        <SubComponentFooter />
      </div>
    );
  }
}
export default Faqs;
