import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import Config from "../config/config";
import "../css/footer.css";
class SubComponentFooter extends Component {
  render() {
    return (
      <div className="secondFooterContent">
        <div className="updates displayflexcenter">
          <div className="displayflexbetween">
            <img src={require("../images/IconLogo-Bottom.svg")} alt="IconLogo" className="updateLogo"/>
            <span>Never miss a story from <b>dreamtech.io</b>, when you sign up. <br/><Link to="/404" className="learnMore">Learn more</Link></span>
          </div>
          <button className="getUpdatesButton">GET UPDATES</button>
        </div>
        <div className="subComponentFooter">
          <ul className="displayflexcenter">
            <li><Link to="/">Home</Link></li>
            <li><a href="https://file.bitdtc.com/whitepaper.pdf" target="_blank" rel="noopener noreferrer">White Paper</a></li>
            <li><Link to="/aboutUs">About Us</Link></li>
            <li><Link to="/faqs">FAQ</Link></li>
          </ul>
          <h5>Have some questions? Please contact us at <a href={Config.dreamTech.url} target="_blank">DreamTech</a></h5>
          <h4>© 2018 DreamTech. All rights reserved.</h4>
        </div>
      </div>
    );
  }
}

export default SubComponentFooter;
