import React, {Component} from 'react';
import $ from 'jquery';
import history from '../js/history';
import {message, Modal} from "antd";
import {Link} from 'react-router-dom';
import jwt from "jsonwebtoken";
import "../css/header.css";

const mapList = [
  {name: "Bahasa Melayu", key: "ms", icon: require("../images/flag/Bahasa Melayu.svg")},
  {name: "ភាសាខ្មែរ", key: "km", icon: require("../images/flag/Khmer.svg")},
  {name: "Deutsch", key: "de", icon: require("../images/flag/Deutsch.svg")},
  {name: "Bahasa Indonesia", key: "id", icon: require("../images/flag/Bahasa Indonesia.svg")},
  {name: "Русский", key: "ru", icon: require("../images/flag/Russian.svg")},
  {name: "English", key: "en", icon: require("../images/flag/English.svg")},
  {name: "Italiano", key: "it", icon: require("../images/flag/Italiano.svg")},
  {name: "Svenska", key: "sv", icon: require("../images/flag/Svenska.svg")},
  {name: "台湾", key: "zh-TW", icon: require("../images/flag/Taiwan.png")},
  {name: "Nederlands", key: "nl", icon: require("../images/flag/Nederlands.svg")},
  {name: "ไทย", key: "th", icon: require("../images/flag/Thailand.svg")},
  {name: "Filipino", key: "tl", icon: require("../images/flag/Filipino.svg")},
  {name: "日本语", key: "ja", icon: require("../images/flag/Japan.svg")},
  {name: "Türkçe", key: "tr", icon: require("../images/flag/Turkish.svg")},
  {name: "Français", key: "fr", icon: require("../images/flag/Français.svg")},
  {name: "Norsk", key: "no", icon: require("../images/flag/Norsk.svg")},
  {name: "Tiếng Việt", key: "vi", icon: require("../images/flag/Vietnam.svg")},
  {name: "한국어", key: "ko", icon: require("../images/flag/Korean.svg")},
  {name: "Polski", key: "pl", icon: require("../images/flag/Polski.svg")}
];

class ComponentHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogined: false,
      visible: false
    };
  };
  /*组件加载完成之后查看localStorage是否存在用户名*/
  componentDidMount() {
    this.goToConfirmedPage();
    this.googleLogin();
    this.userLogined();
    setTimeout(function () {
      $(window).scrollTop(0)
    }, 10);
  }
  getQueryString = (name) => {
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    let r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
  };
  goToConfirmedPage = () => {
    let params = this.getQueryString("confirm");
    if (params) {
      history.push(`/confirmed/${params}`);
    }
  };
  googleLogin = () => {
    let params = this.getQueryString("token");
    // window.location.hash = "demo.bitdtc.com";
    if (params) {
      const decoded = jwt.decode(params, {complete: true});
      for (let item in decoded.payload) {
        localStorage.setItem(item, decoded.payload[item]);
      }
      localStorage.setItem("token", params);
      if(decoded["payload"]["passwordStatus"]){
        message.success('Login Successful!');
        setTimeout(function () {
          history.push(`/`);
        },2000);
      }else{
        message.success('Signed in with Google Successful! ');
        this.showModal();
        history.replace("/");


      }
    }
  };

  goToSecuritySettings = () => {
    history.push(`/security`);
  };
  /*判断用户是否登陆*/
  userLogined = () => {
    let username = localStorage["Email"];
    if (username) {
      this.setState({
        isLogined: true,
        userName: username
      });
      this.interval();
    }
  };
  /*token过期*/
  interval = () => {
    setInterval(() => {
      let expired = localStorage.getItem('exp');
      let unixTimestamp = expired * 1000;
      let datenow = new Date().getTime();
      if (unixTimestamp < datenow) {
        this.handleLogout();
      }
    }, 1000);
  };
  /*登出*/
  handleLogout = () => {
    clearInterval(this.interval);
    this.setState({
      isLogined: false
    });
    localStorage.clear();
    console.clear();
    message.success('Logout Successful!');
    setTimeout(function () {
      history.push("/");
      window.location.reload();
    }, 300);
  };
  refreshPage = () => {
    history.push("/");
    setTimeout(function () {
      $(window).scrollTop(0)
    }, 10);
  };
  showModal = () => {
    this.setState({
      visible: true
    });
  };
  handleCancel = (e) => {
    this.setState({
      visible: false
    });
  };
  popUpId = (lang) => {
    let selectField = document.querySelector("#google_translate_element select");
    for (let i = 0; i < selectField.children.length; i++) {
      let option = selectField.children[i];
      if (option.value === lang) {
        window.googleTranslateElementInit = this.googleTranslateElementInit;
        selectField.selectedIndex = i;
        selectField.dispatchEvent(new Event('change'));
        break;
      }
    }
  };
  render() {
    let loginedHeader = this.state.isLogined
      ? <div className="displayflexbetween">
        <Link to={`/accountCenter`} className="myAccount">My Account</Link>
        <a onClick={this.handleLogout} className="logout" title="Logout">
          <img src={require("./../images/logoutIcon.svg")} alt="Logout"/>
        </a>
      </div>
      : <div className="displayflexbetween">
        <Link className="login" to={`/loginregister/${"login"}`}>LOGIN</Link>
        <Link className="signup" to={`/loginregister/${"register"}`}>SIGN UP</Link>
      </div>;
    return (
      <div>
        <div className="componentHeader displayflexbetween" id="node">
          <img src={require("../images/logoSample2-01.svg")} alt="logo" onClick={this.refreshPage}/>
          <ul className="displayflexbetween">
            <li onClick={this.refreshPage}><a>HOME</a></li>
            <li><Link to='/aboutUs'>ABOUT US</Link></li>
            <li><Link to="/faqs">FAQ</Link></li>
            <li><a href="https://file.bitdtc.com/whitepaper.pdf" target="_blank" rel="noopener noreferrer">WHITEPAPER</a>
            </li>
            <li className="moreButton"><a>MORE</a> <span className="fa fa-angle-down"> </span>
              <div id="moreDownMenu">
                <a href="#insideScoop" rel="nofollow">Subscribe</a>
                <a href="#asfeaturedOn" rel="nofollow">Contact Us</a>
                <a href="https://file.bitdtc.com/lightpaper.pdf" target="_blank" rel="noopener noreferrer">Light Paper</a>
              </div>
            </li>
            <li className="languageButton"><a className="map-globe"> </a> <span className="fa fa-angle-down"> </span>
              <div id="languageDownMenu">{
                  mapList.map((item, index) => {
                    return (
                      <a key={index} className="displayflexbetween" onClick={() => this.popUpId(item.key)}>
                        <img src={item.icon} alt="flag"/>
                        <span>{item.name}</span>
                      </a>
                    )
                  })
                }
              </div>
            </li>
            <div id="google_translate_element"> </div>
            {loginedHeader}
          </ul>
        </div>
        <div className="popupWindow">
          <Modal visible={this.state.visible}
                 footer={null}
                 maskClosable={false}
                 onCancel={this.handleCancel}>
            <div className="popupWindowContent">
              <img src={require("./../images/logoSample2-01.svg")} alt="logo"/>
              <div className="userInformation">
                <h3>We have sent a temporary password to <br/><b className="tempPass">{localStorage["Email"]}</b></h3>
                <h4>Please change your password <a onClick={this.goToSecuritySettings}>here</a></h4>
                {/* <h4>We got your message and will get back to <br/> you soon as we can.</h4> */}
              </div>
              <a>Follow us on:</a>
              <div className="displayflexcenter">
                <img src={require("./../images/followuson/Google+.svg")} alt="facebook"/>
                <img src={require("./../images/followuson/facebook.svg")} alt="facebook"/>
                <img src={require("./../images/followuson/instagram.svg")} alt="facebook"/>
                <img src={require("./../images/followuson/twitter.svg")} alt="facebook"/>
              </div>
              <h2> </h2>
              <p>© 2018 DreamTech. All rights reserved.</p>
            </div>
          </Modal>
        </div>
      </div>
    );
  }
}

export default ComponentHeader;
