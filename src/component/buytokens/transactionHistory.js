import React, {Component} from 'react';
import {Table, Badge} from 'antd';
import message from "antd/lib/message";
import Config from "../../config/config";
import {createApolloFetch} from "apollo-fetch";
import "../../css/buytokens/transactionHistory.css";

let uri = Config.api;
let apolloFetch = createApolloFetch({ uri });

class NestedTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rate: '',
      code: '',
      historyList:[],
    }
  }
  componentDidMount(){
    this.fetchHistory();
    this.getRate();
  };
  timeFormat=(str)=>{
    if(str){
      let arr = str.split("+");
      return arr[0]
    }
  };

  authorizeToken = () => {
    apolloFetch.use(({ request, options }, next) => {
      if (!options.headers) {
        options.headers = {};  // Create the headers object if needed.
      }
      options.headers['authorization'] = localStorage.getItem("token");
      next();
    });
  }

  getRate = () => {
    let query = `query{
                    getTokenContract{
                      rate,code
                    }
                  }`;
    this.authorizeToken();
    apolloFetch({query}).then((response) => {
      if(!response["errors"]){
        this.setState({
          rate: response["getTokenContract"].rate,
          code: response["getTokenContract"].code
        });
      }else {
          console.log("Get rate failed!")
      }
    })
  }
  fetchHistory = () => {
    let query = `query{history{description,amount,date,rate}}`;
    let that=this;
    this.authorizeToken();
    apolloFetch({query}).then(function (response) {
      if (!response["errors"]) {
        let historyData=response["history"];
        let compare=function(obj1,obj2){
          let a=new Date(obj1.date);
          let b=new Date(obj2.date);
          if (a > b) {
            return -1;
          } else if (a < b) {
            return 1;
          } else {
            return 0;
          }
        };
        that.setState({
          historyList:historyData.sort(compare)
        })
      } else {
        console.log(response);
        message.error(response.errors[0].message);
      }
    })
  };
  // expandedRowRender = () => {
  //   const columns = [
  //     {title: 'OrderID', dataIndex: 'id', key: 'id'},
  //     {title: 'You will receive:', dataIndex: 'receive', key: 'receive'},
  //     {title: 'Order will expire on:', dataIndex: 'expire', key: 'expire'},
  //     {title: 'Send 0.001289612 ETH to ', dataIndex: 'Send', key: 'Send'},
  //   ];
  //   const data = [];
  //   for (let i = 0; i < 1; ++i) {
  //     data.push({
  //       key: i,
  //       id: '',
  //       receive: '',
  //       expire: '',
  //       Send: '',
  //     });
  //   }
  //   return (
  //     <Table columns={columns} dataSource={data} pagination={false}/>
  //   );
  // };
  render() {
    const columns = [
      {title: 'No.', dataIndex: 'key', key: 'key'},
      {title: 'Date', dataIndex: 'date', key: 'date'},
      {title: 'Description', dataIndex: 'description', key: 'description'},
      {title: 'Status', dataIndex: 'version', key: 'version', render: () => <Badge status="success" text="success"/>},
      {title: 'Amount', dataIndex: 'amount', key: 'amount'},
      {title: 'Rate', dataIndex: 'rate', key: 'rate'},
      // {title: 'Type', key: 'operation', render: () => <a className="fa fa-redo"> </a>},
    ];
    const data = [];
    let historyList=this.state.historyList;
    for (let i = 0; i < historyList.length; ++i) {
      data.push({
        key: i + 1,
        date: this.timeFormat(historyList[i].date),
        description: historyList[i].description,
        amount: historyList[i].amount,
        // rate: historyList[i].rate,
        rate: `${this.state.rate} ${this.state.code} = 1 ETH`,
        status: "success"
      });
    }
    return (
      <div className="transactionHistory">
        <Table
          className="components-table-demo-nested"
          columns={columns}
          // expandedRowRender={this.expandedRowRender}
          dataSource={data}
        />
      </div>
    );
  }
}

export default NestedTable;