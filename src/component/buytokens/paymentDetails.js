import React, {Component} from 'react';
import Config from "../../config/config";
import {createApolloFetch} from "apollo-fetch";
import message from "antd/lib/message";
import $ from 'jquery';
import "../../css/buytokens/dashboard.css";

class PayMentDetails extends Component {
  constructor(props) {
    super(props);
    this.state={
      total:"0.001",
    }
  }
  handleConforimBtn=()=>{
    this.props.showModal();
    let hashArr=[];
    $(".hashContent .walletAddress").each(function(i,e){
      console.log(e.value);
      hashArr.push(e.value);
    });
    console.log(hashArr);
    this.sendDetail(hashArr);
  };
  sendDetail=(hashArr)=>{
    let that = this;
    let uri = Config.api;
    let apolloFetch = createApolloFetch({ uri });
    let query = `mutation($transaction_hash:[String]!,$total:String!){BuyToken(hash:$transaction_hash,total:$total)}`;
    let variables = {
      total:this.props.amountInETH,
      transaction_hash:hashArr
    };
    setTimeout(function(){
      apolloFetch.use(({ request, options }, next) => {
        if (!options.headers) {
          options.headers = {};  // Create the headers object if needed.
        }
        options.headers['authorization'] = localStorage.getItem("token");
        next();
      });
      apolloFetch({query,variables}).then(function (response) {
        if (!response["errors"]&&response["BuyToken"]) {
          that.props.fetchWalletAddress();
          that.props.changeStep(2);
          setTimeout(function(){
            window.location.reload();
          }, 800)
        } else {
          that.props.changeStep(3);
          message.error(response.errors[0].message);
        }
      })
    },3000);
  };
  render() {
    return (
        <div>
          <ul className="amountBonus" >
            <li>
              <em> Please send: </em>
              <span>{this.props.amountInETH}ETH  <i> to </i> {this.props.tokenContract["address"]}</span>
            </li>
            <li>
              <em> You will receive: </em>
              <span>{this.props.amountInETH * this.props.tokenContract["rate"]} DTC</span>
            </li>
          </ul>
          <div  style={{width: "600px", margin: "30px auto 0"}}>
            <h6>Transaction Hash</h6>
            <div className="hashContent">
              <input className="walletAddress" placeholder="Paste the transaction hash here ..."/>
            </div>
            {/* <a className="addHash" onClick={()=>$(".hashContent").append('<input class="walletAddress" />')}> Add new transaction hash </a> */}
          </div>
          <span className="btns confirmBtn" onClick={this.handleConforimBtn}>CONFIRM</span>
          <span className="btns backBtn" onClick={this.props.goBack}>BACK</span>
        </div>
    );
  }
}

export default PayMentDetails;
