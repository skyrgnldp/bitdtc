import React, {Component} from 'react';
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import message from "antd/lib/message";
import "../../css/buytokens/dashboard.css";


class BuyTokens extends Component {
  constructor(props) {
    super(props);
    this.state = {
      amountInETH: ""
    }
  }
  goToBuyTokensDetial=(e)=>{
    e.preventDefault();
    if(isNaN(this.props.amountInETH)){
      message.error("Amount ETH you entered is not a number!")
    }else if(this.props.amountInETH === ""){
      message.error("Amount ETH field is required!")
    }else if(this.props.amountInETH.trim() === ""){
      message.error("Strictly avoid spaces!")
    }else if(this.props.amountInETH < this.props.tokenContract["minimum"]){
      message.error(`Buying token has minimum of ${this.props.tokenContract["minimum"]} ETH`)
    }

    else{
      this.props.setBuyTokens();
    }
  };
  render() {
    return (
      <div>
        {/*<div className="bonus">Current Bonus: <em>{this.props.bonus}%</em></div>*/}
        <div style={{width: "600px", margin: "30px auto"}}>
          <Form onSubmit={this.goToBuyTokensDetial}
                className="displayflexcenter"
                ref="getSwordButton"
                autoComplete="OFF">
            <span className="ETH">ETH</span>
            <Input type="text"
                   placeholder="Amount in ETH"
                   name="amountInETH"
                   onKeyUp={this.props.vauleChange}/>
            <Button type="primary"
                    htmlType="submit"
                    className="buyToken">Buy Token</Button>
          </Form>
          <span className="ETHWallet">to ETH Wallet(min {this.props.tokenContract["minimum"]} ETH)</span>
          <h6>Recipient Wallet Address</h6>
          <div className="walletAddress">{this.props.tokenContract["address"]}</div>
          <h6>Contract</h6>
          <div className="walletAddress">{this.props.tokenContract["contract"]}</div>
          <div className="senderWalletContent">
            <h5>DREAMTECH CASH</h5>
            <ul>
              <li>
                <span><img src={require("../../images/dollar-01.svg")} alt="USD"/> USD</span>
                <span>{"0.5"}</span>
              </li>
              <li>
                <span><img src={require("../../images/bitCoin.svg")} alt="BTC"/> BTC</span>
                <span>{"0.0000079"}</span>
              </li>
              <li>
                <span><img src={require("../../images/ethereum_1-01.svg")} alt="ETH"/> ETH</span>
                <span>{"0.001771"}</span></li>
              <li>
                <span><img src={require("../../images/MOC Logo.svg")} alt="MOC"/> MOC</span>
                <span>{"0.000412"}</span>
              </li>
            </ul>
            <h5>OTHER RATES</h5>
            <ul>
              <li>
                <span><img src={require("../../images/dash.svg")} alt="DASH"/> DASH</span>
                <span>{"0.00331"}</span>
              </li>
              <li>
                <span><img src={require("../../images/litecoin-01.svg")} alt="LTC"/> LTC</span>
                <span>{"0.00908"}</span>
              </li>
              <li>
                <span><img src={require("../../images/zcash.svg")} alt="ZEC"/> ZEC</span>
                <span>{"0.003648"}</span>
              </li>
              <li>
                <span><img src={require("../../images/digiX.svg")} alt="DXC"/> DXC</span>
                <span>{"0.000412"}</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default BuyTokens;
