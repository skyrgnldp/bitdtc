import React, {Component} from 'react';
import SubComponentHeader from '../subComponentHeader';
import SubComponentFooter from '../subComponentFooter';
import Dashboard from './dashboard';
import Checkout from './checkout';
import TransactionHistory from './transactionHistory';
import WalletCenter from './walletCenter';
import history from '../../js/history';
import {createApolloFetch} from "apollo-fetch";
import message from "antd/lib/message";
import Config from "../../config/config";
import "../../css/buytokens/accountCenter.css";

let uri = Config.api;
let apolloFetch = createApolloFetch({ uri });

class AccountCenter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalDtc:0,
      selectedTab:"1",
      yourWalletAddress:"",
      tokenContract:{},
    }
  }
  componentWillMount(){
    const email = localStorage["Email"];
    if (!email){
      history.push(`/`);
    }
  }
  componentDidMount(){
    this.fetchWalletAddress();
    this.fetchTokenContract();
  };


  authorizeToken=()=>{
    apolloFetch.use(({ request, options }, next) => {
      if (!options.headers) {
        options.headers = {};  // Create the headers object if needed.
      }
      options.headers['authorization'] = localStorage.getItem("token");
      next();
    });
  };
  /*获得wallet address*/
  fetchWalletAddress=()=>{
    let query = `query{getWallet{ address,total }}`;
    let that= this;
    this.authorizeToken();
    apolloFetch({query}).then((response) => {
      if (!response["errors"]) {
        that.setState({
          yourWalletAddress:response["getWallet"].address,
          totalDtc:response["getWallet"].total,
        });
      } else {
        message.error(response.errors[0].message);
      }
    })
  };
  /*获得wallet address*/
  fetchTokenContract=()=>{
    let query = `query{getTokenContract{minimum,address,name,code,rate,contract}}`;
    let that= this;
    this.authorizeToken();
    apolloFetch({query}).then(function (response) {
      console.log(response);
      if (!response["errors"]) {
        that.setState({
          tokenContract:response["getTokenContract"]
        });
      } else {
        message.error(response.errors[0].message);
      }
    })
  };
  tabChange=(key)=>{
    this.setState({
      selectedTab:key
    })
  };
  render() {
    let {selectedTab} = this.state;
    return (
      <div className="accountCenter">
        <SubComponentHeader/>
        <div className="accountCenterContent">
          <div className="totalDtc">
            <h3>{this.state.totalDtc} DTC</h3>
            <a>DreamTech Cash Wallet</a>
          </div>
          <div className="tabsContent">
            <ul className="displayflexbetween">
              <li onClick={()=>this.tabChange("1")} className={selectedTab === "1" ? 'active' : ''}>WALLET CENTER</li>
              <li onClick={()=>this.tabChange("2")} className={selectedTab === "2" ? 'active' : ''}>DASHBOARD</li>
              <li onClick={()=>this.tabChange("3")} className={selectedTab === "3" ? 'active' : ''}>CHECK OUT</li>
              <li onClick={()=>this.tabChange("4")} className={selectedTab === "4" ? 'active' : ''}>TRANSACTION HISTORY</li>
            </ul>
            <div style={selectedTab === "1" ? {display:"block"} : {display:"none"}}>
              <WalletCenter walletList={this.state.yourWalletAddress} />
            </div>
            <div style={selectedTab === "2" ? {display:"block"} : {display:"none"}}>
              <Dashboard tokenContract={this.state.tokenContract} fetchWalletAddress={this.fetchWalletAddress}/>
            </div>
            <div style={selectedTab === "3" ? {display:"block"} : {display:"none"}}>
              <Checkout tokenContract={this.state.tokenContract} fetchWalletAddress={this.fetchWalletAddress}/>
            </div>
            <div style={selectedTab === "4" ? {display:"block"} : {display:"none"}}>
              <TransactionHistory/>
            </div>
          </div>
        </div>
        <SubComponentFooter/>
      </div>
    );
  }
}

export default AccountCenter;
