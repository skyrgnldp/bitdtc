import React, {Component} from 'react';
import message from "antd/lib/message";
import Config from "../../config/config";
import {createApolloFetch} from "apollo-fetch";
import "../../css/buytokens/walletCenter.css";

class WalletCenter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addressValue: '',
      walletList: props.yourWalletAddress,
    }
  }

  onChangeAddress = (e) => {
    this.setState({
      addressValue: e.target.value
    });

  };

  clearWalletInput = () => {
    this.refs.walletAddressInput.value = null;
  }
  handleConfirm = (e) => {
    e.preventDefault();
    if (this.refs.walletAddressInput.value.trim() === "") {
      message.error('Wallet address cannot be empty! ');
      this.clearWalletInput();
    }else if(this.refs.walletAddressInput.value === this.props.walletList){
      message.error("You cannot enter your current Wallet Address!");
      this.clearWalletInput();
    }
    else {
      this.checkWalletAddress();
    }
  };
  checkWalletAddress = () => {
    let uri = Config.api;
    let apolloFetch = createApolloFetch({uri});
    let query = `mutation($address:String!) { saveWallet( address:$address)}`;
    let that = this;
    let variables = {
      address: that.refs.walletAddressInput.value,
    };
    apolloFetch.use(({request, options}, next) => {
      if (!options.headers) {
        options.headers = {};
      }
      options.headers['authorization'] = localStorage.getItem("token");
      next();
    });
    apolloFetch({query, variables}).then(function (response) {
      console.log(response);
      if (!response["errors"] && response["saveWallet"]) {
        that.setState({
          walletList: that.refs.walletAddressInput.value
        });
        message.success('Wallet Added Successfully!');
        setTimeout(function() {
          window.location.reload();
        }, 800)
      } else {
        message.error(response.errors[0].message);
        that.clearWalletInput();
      }
    })
  };
  keypress = (e) => {
    if (e.which !== 13) return
    this.handleConfirm();
  };

  render() {
    const display = this.props.walletList
      ? <div >
        <div className="walletContent displayflexcenter">
          <input type="text" className="walletAddressText" value={this.props.walletList} disabled={true}/>
        </div>
        <div className="displayflexcenter curWallet">
          <p>Current Wallet Address</p>
        </div>
        <div className="displayflexcenter">
          <input type="text"
                 className="walletInputBox"
                 ref="walletAddressInput"
                 placeholder="Please enter your new wallet address"
                 // onChange={this.onChangeAddress}
                 onKeyPress={this.keypress}/>
          <button className="addWallet" disabled={this.state.disable} onClick={this.handleConfirm}>Replace Wallet</button>
        </div>
      </div>
      : <div className="displayflexcenter">
          <input type="text"
                 ref="walletAddressInput"
                 className="walletInputBox"
                 placeholder="paste your wallet here*"
                 // onChange={this.onChangeAddress}
                 onKeyPress={this.keypress}/>
          <span className="addWallet" onClick={this.handleConfirm}>Add Wallet</span>
      </div>;

    //TEST PURPOSE
    // const display = this.props.walletList ?
    // <div className="displayflexcenter">
    //    <input type="text"
    //           ref="walletAddressInput"
    //           className="walletInputBox"
    //           placeholder="paste your wallet here*"
    //           onKeyUp={this.onChangeAddress}
    //           onKeyPress={this.keypress}/>
    //    <span className="addWallet" onClick={this.handleConfirm}>Add Wallet</span>
    //  </div> :
    //  <div >
    //    <div className="walletContent displayflexcenter">
    //      <input type="text" className="walletAddressText" value={this.props.walletList} disabled={true}/>
    //    </div>
    //    <div className="displayflexcenter">
    //      <input type="text"
    //             className="walletInputBox"
    //             ref="walletAddressInput"
    //             placeholder="Please enter your new wallet address"
    //             onKeyUp={this.onChangeAddress}
    //             onKeyPress={this.keypress}/>
    //      <button className="addWallet" onClick={this.handleConfirm}>Edit Wallet</button>
    //    </div>
    //  </div>;

    return (
      <div className="accountAddress">
        <div className="accountAddressContent">
          <h5>Wallet Address</h5>
          {display}
        </div>
      </div>
    );
  }
}

export default WalletCenter;
