import React, {Component} from 'react';
import PayMentDetails from "./paymentDetails";
import BuyTokens from "./buyTokens";
import {Modal} from "antd";
import Config from "../../config/config";
import "../../css/popupwindow/popupwindow.css";
import "../../css/buytokens/dashboard.css";


class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bonus: 0,
      walletList:this.props.walletList,
      buytokens:false,
      visible: false,
      step:1,
      amountInETH:""
    }
  }
  showModal = () => {
    this.setState({
      visible: true
    });
  };
  handleCancel = () => {
    let that=this;
    this.setState({
      visible: false
    });
    setTimeout(function(){
      that.setState({
        step: 1
      });
    },1000);
  };
  setBuyTokens=()=>{
    this.setState({
      buytokens:true,
      walletList:this.props.walletList
    });
  };
  goBack=()=>{
    this.setState({
      buytokens:false,
    });
  };
  changeStep=(step)=>{
    this.setState({
      step:step
    });
  };
  vauleChange = (e) => {
    this.setState({
      amountInETH: e.target.value
    });
  };
  render() {
    return (
      <div className="dashboard">
        <div className="nav">
          <span>Buy Token </span>
          <span className="fa fa-angle-right"> </span>
          <span>Payment Details</span>
        </div>
        <div style={this.state.buytokens ? {display:"none"}:{display:"block"}}>
          <BuyTokens bonus={this.state.bonus}
                     setBuyTokens={this.setBuyTokens}
                     vauleChange={this.vauleChange}
                     amountInETH={this.state.amountInETH}
                     tokenContract={this.props.tokenContract}/>
        </div>
       <div style={this.state.buytokens ? {display:"block"}:{display:"none"}}>
         <PayMentDetails goBack={this.goBack}
                         tokenContract={this.props.tokenContract}
                         showModal={this.showModal}
                         amountInETH={this.state.amountInETH}
                         fetchWalletAddress={this.props.fetchWalletAddress}
                         changeStep={this.changeStep}/>
       </div>
        <Modal visible={this.state.visible}
               footer={null}
               closable={false}
               maskClosable={false}
               onCancel={this.handleCancel}>
          <div className="popupWindowContent">
            <img src={require("../../images/logoSample2-01.svg")} alt="logo" className="logo"/>
            <div style={this.state.step === 1 ? {display:"block"}:{display:"none"}}>
              <a className="checkingTransaction">Checking Transaction</a>
              <img src={require("../../images/DTC-LoadingGIF.gif")} className="loading" alt="loading"/>
              <p className="displayflexcenter">Please wait ...</p>
            </div>
            <div className="response" style={this.state.step === 2 ? {display:"block"}:{display:"none"}}>
              <a className="checkingTransaction">Transaction Successful.</a>
              <span onClick={this.handleCancel}>Continue</span>
            </div>
            <div className="response" style={this.state.step === 3 ? {display:"block"}:{display:"none"}}>
              <a className="checkingTransaction">Transaction Failed.</a>
              <span onClick={this.handleCancel}>Back</span>
            </div>
            <h2>Have some questions? Please contact us at <a href={Config.dreamTech.url} target="_blank">Dreamtech</a></h2>
            <p>© 2018 DreamTech. All rights reserved.</p>
          </div>
        </Modal>
      </div>
    );
  }
}

export default Dashboard;
