import React, {Component} from 'react';
import {createApolloFetch} from "apollo-fetch";
import message from "antd/lib/message";
import Config from "../../config/config";
import {Modal} from "antd";
import "../../css/popupwindow/popupwindow.css";
import "../../css/buytokens/checkout.css";

let uri = Config.api;
let apolloFetch = createApolloFetch({ uri });
message.config({
  top: 50,
  duration: 2,
  maxCount: 1,
});
class Checkout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      info: "",
      amount: "",
      visible: false,
      step:1
    }
  }
  changeStep=(step)=>{
    this.setState({
      step:step
    });
  };
  showModal = () => {
    this.setState({
      visible: true
    });
  };
  handleCancel = () => {
    let that=this;
    this.setState({
      visible: false
    });
    setTimeout(function(){
      that.setState({
        step: 1
      });
    },1000);
  };
  changeVal = (e) => {
    let val = e.target.value;
    this.setState({
      amount: val,
      info: ""
    });
  };
  keypress = (e) => {
    if (e.which !== 13) return
    this.calculate();
  };

  authorizeToken = () => {
    apolloFetch.use(({ request, options }, next) => {
      if (!options.headers) {
        options.headers = {};  // Create the headers object if needed.
      }
        options.headers['authorization'] = localStorage.getItem("token");
        next();
    });
  }
  calculate = () => {
    if (isNaN(this.state.amount)||this.state.amount==="") {
      message.error("Please input valid amount number!")
      this.refs.amountVal.value = "";
    } else if (this.state.amount === 0 || this.state.amount === "0" || this.state.amount <= 0){
      message.error("Please input a valid amount!")
      this.refs.amountVal.value = "";
    }
    else {
      let that = this;
      let query = `mutation($amount:String!){Withdraw(amount:$amount){message,errors{Path,Message}}}`;
      let variables = {
        amount: this.state.amount
      };
      this.showModal();
      this.authorizeToken();
      apolloFetch({query, variables}).then((response) => {
        console.log(response);
        if (response["Withdraw"].message) {
          that.setState({
            amount: "",
          });
          that.changeStep(2);
          that.props.fetchWalletAddress();
          setTimeout(function(){
            window.location.reload();
          }, 800)

        } else {
          that.changeStep(3);
          message.error(response["Withdraw"].errors[0]["Message"]);
        }
      });
    }
  };
  render() {
    return (
      <div className="checkout">
        <div className="checkoutleft">
          <div className="displayflexcenter">
            <div className="inner">
              <h6>Amount DTC</h6>
              <div className="inputContent">
                <input type="text"
                       value={this.state.amount}
                       onChange={this.changeVal}
                       onKeyPress={this.keypress}
                       ref="amountVal"
                       placeholder="Please enter your totally "/><span>DTC</span>
              </div>
            </div>
            <span className="fa"> = </span>
            <div className="inner">
              <h6>Result ETH</h6>
              <div className="cl-3">{this.state.amount / this.props.tokenContract["rate"]} <span>ETH</span></div>
            </div>
            <div className="inner">
              <h6>Currency DTC / 1 ETH</h6>
              <div className="cl-3">10000 <span>DTC / 1 ETH</span></div>
            </div>

          </div>
        </div>
        <p className="info">{this.state.info}</p>
        <div className="displayflexcenter" style={{height: "30px"}}>
          <button className="reset" onClick={this.calculate}>Withdraw</button>
        </div>
        <Modal visible={this.state.visible}
               footer={null}
               closable={false}
               maskClosable={false}
               onCancel={this.handleCancel}>
          <div className="popupWindowContent">
            <img src={require("../../images/logoSample2-01.svg")} alt="logo" className="logo"/>
            <div className="response" style={this.state.step === 1 ? {display:"block"}:{display:"none"}}>
              <a className="checkingTransaction">Checking Out</a>
              <img src={require("../../images/DTC-LoadingGIF.gif")} className="loading" alt="loading"/>
              <p className="displayflexcenter">Please wait ...</p>
            </div>
            <div className="response" style={this.state.step === 2 ? {display:"block"}:{display:"none"}}>
              <a className="checkingTransaction">Successfully Checked Out!</a>
              <i className="fa fa-check" style={{fontSize:"32px",color:"#00D600"}}> </i>
              <span onClick={this.handleCancel}>Back</span>
            </div>
            <div className="response" style={this.state.step === 3 ? {display:"block"}:{display:"none"}}>
              <a className="checkingTransaction">Check Out Failed!</a>
              <i className="fa fa-times" style={{fontSize:"32px",color:"#FF172E"}}> </i>
              <span onClick={this.handleCancel}>Back</span>
            </div>
            <h2>Have some questions? Please contact us at <a href={Config.dreamTech.url} target="_blank">Dreamtech</a></h2>
            <p>© 2018 DreamTech. All rights reserved.</p>
          </div>
        </Modal>
      </div>
    );
  }
}

export default Checkout;
