import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import "../css/footer.css";

class ComponentFooter extends Component {
  render() {
    return (
      <div className="componentFooter">
        <div className="displayflexbetween">
          <p>Copyright © 2017 DREAM TECH CASH - by Dream Tech. All rights reserved.</p>
          <ul className="displayflexend">
            <li><a href="https://file.bitdtc.com/whitepaper.pdf" target="_blank" rel="noopener noreferrer">White Paper</a></li>
            <li><Link to="/aboutUs">About us</Link></li>
            {/*<li><Link to="">Terms</Link></li>*/}
            <li><Link to="/faqs">FAQ</Link></li>
          </ul>
        </div>
      </div>
    );
  }
}

export default ComponentFooter;
