import React, {Component} from 'react';
import SubComponentHeader from '../subComponentHeader';
import SubComponentFooter from '../subComponentFooter';
import history from "../../js/history";
import Config from "../../config/config";
import {createApolloFetch} from "apollo-fetch";
import message from "antd/lib/message";
import "../../css/accountCenter/personalSecurity.css";

let uri = Config.api;
const apolloFetch = createApolloFetch({uri});

class SecuritySettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oldPassword: "",
      newPassword: "",
      confirmPassword: "",
      errorMsg: "",
    }
  }
  componentWillMount(){
    const email = localStorage["Email"];
    if (!email){
      history.push(`/`);
    }
  }

  componentDidMount(){
    this.getTotalDtc();
  }

  authorizeToken = () => {
    apolloFetch.use(({request,options}, next) => {
      if(!options.headers){
        options.headers = {};
      }
        options.headers["authorization"] = localStorage["token"];
        next();
    })
  };

  getTotalDtc = () => {
    let query = `query{
                    getWallet{
                      total
                    }
                  }`;
    this.authorizeToken();
    apolloFetch({query}).then((response) => {
      if(!response["errors"]){
        this.setState({
          totalDtc: response["getWallet"].total
        });
      }else {
        message.error(response.errors[0].message)
      }
    })
  };
  vauleChange = (e) => {
    this.setState({
      errorMsg: "",
      [e.target.name]: e.target.value
    });
  };
  handleEnter=(e)=>{
    if (e.which !== 13) return;
    this.changePassword();
  };
  checkInput = (obj) => {
    for (let item in obj) {
      if (obj[item] === "") {
        this.setState({
          errorMsg: "Please enter your " + item + "."
        });
        return false
      }
    }
    return true
  };
  changePassword=()=>{
    let userInfo = {
      oldPassword: this.state.oldPassword,
      newPassword: this.state.newPassword,
      confirmPassword: this.state.confirmPassword
    };
    if (this.checkInput(userInfo)) {
      if (userInfo.newPassword === userInfo.confirmPassword) {
        this.sendRegisterInfo(userInfo);
      } else {
        this.setState({
          errorMsg: message.error("The new password and confirm password are not the same, please re-enter!")
        });
        this.refs.oldpass.value = "";
        this.refs.newpass.value = "";
        this.refs.confirmpass.value = "";
        return
      }
    }
  };
  sendRegisterInfo = (userInfo) => {
    let query = `mutation($oldPassword:String!,$newPassword:String!) { SavePassword( oldPassword:$oldPassword,newPassword:$newPassword ){errors {Path,Message} }}`;
    let variables = {
      oldPassword: userInfo.oldPassword,
      newPassword: userInfo.newPassword
    };
    this.authorizeToken();
    apolloFetch({query, variables}).then((response) => {

      console.log(response);
      if (response["SavePassword"]["errors"] === null && !response["errors"]) {
        message.success("Your password has been changed successfully. Please login with your new password!");
        setTimeout(function(){
          localStorage.clear();
        },3000);
      } else {
        message.error("Your old password is incorrect, please try again!");
        this.refs.oldpass.value = "";
        this.refs.newpass.value = "";
        this.refs.confirmpass.value = "";
      }
    });
  };
  backToMyAccount=()=>{
    history.push("/accountCenter");
  };
  render() {
    return (
      <div className="security">
        <SubComponentHeader/>
        <div className="securityPersonalContent">
          <div className="totalDtc">
            <h3>{this.state.totalDtc} DTC</h3>
            <a className="">DreamTech Cash Wallet</a>
          </div>
          <div style={{width:"75%",margin:"30px auto"}}>
            <h4>Security Settings</h4>
            <div className="editable">
              <h5>Old Password</h5>
              <input type="password"
                     name="oldPassword"
                     ref="oldpass"
                     onChange={this.vauleChange}
                     onKeyPress={this.handleEnter}
                     placeholder="enter old password*"/>
              <h5>New Password</h5>
              <input type="password"
                     name="newPassword"
                     ref="newpass"
                     onChange={this.vauleChange}
                     onKeyPress={this.handleEnter}
                     placeholder="enter new password*"/>
              <h5>Re-enter New Password</h5>
              <input type="password"
                     name="confirmPassword"
                     ref="confirmpass"
                     onChange={this.vauleChange}
                     onKeyPress={this.handleEnter}
                     placeholder="re-enter new password*"/>
              <span className="errorMsg">{this.state.errorMsg}</span>
            </div>
            <span className="update" onClick={this.changePassword}>UPDATE</span>
            <span className="back" onClick={this.backToMyAccount}>BACK</span>
          </div>
        </div>
        <SubComponentFooter/>
      </div>
    );
  }
}

export default SecuritySettings;
