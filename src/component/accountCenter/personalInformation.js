import React, {Component} from 'react';
import SubComponentHeader from '../subComponentHeader';
import SubComponentFooter from '../subComponentFooter';
import history from "../../js/history";
import Config from "../../config/config";
import {createApolloFetch} from "apollo-fetch";
import message from "antd/lib/message";
import "../../css/accountCenter/personalSecurity.css";

let uri = Config.api;
let apolloFetch = createApolloFetch({ uri });

class PersonalInformation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalDtc: 0,
      firstName: "",
      lastName: "",
      country:"",
      street: "",
      email: "",
      password: "",
    }
  }
  componentWillMount(){
    const email = localStorage["Email"];
    if (!email){
      history.push(`/`);
    }
  }
  componentDidMount(){
    this.fetchUserInformation();
    this.getTotalDtc();
  };

  authorizeToken = () => {
    apolloFetch.use(({ request, options }, next) => {
      if (!options.headers) {
        options.headers = {};  // Create the headers object if needed.
      }
        options.headers['authorization'] = localStorage.getItem("token");
        next();
    });
  }

  getTotalDtc = () => {
    let query = `query{
                    getWallet{
                      total
                    }
                  }`;
    this.authorizeToken();
    apolloFetch({query}).then((response) => {
      console.log(response)
      if(!response["errors"]){
        this.setState({
          totalDtc: response["getWallet"].total
        });
      }else {
        message.error(response.errors[0].message)
      }
    })
  }


  fetchUserInformation=()=>{
    let that=this;
    let query = `query{
                    Profile{
                      UserID,Username,Email,Roles,Firstname,Lastname,Birthdate,Language,Created,Address {
                        Country,City,Street,State
                       }
                     }
                  }`;
    this.authorizeToken();
    apolloFetch({query}).then(function (response) {
      console.log(response);
      if (!response["errors"]) {
        that.setState({
          firstName: response["Profile"]["Firstname"],
          lastName: response["Profile"]["Lastname"],
          country:response["Profile"]["Address"]["Country"],
          street: response["Profile"]["Address"]["Street"],
          email: response["Profile"]["Email"],
        });
      } else {
        console.log(response);
        message.error(response.errors[0].message);
      }
    })
  };
  backToMyAccount=()=>{
    history.push("/accountCenter");
  };
  vauleChange = (e) => {
    this.setState({
      errorMsg: "",
      [e.target.name]: e.target.value
    });
  };
  updatePersonalInformation=()=>{
    let query = `mutation($input:ProfileFields!,$password:String!) {
                  SaveProfile( input:$input,password:$password ) {
                    message,errors {Path,Message}
                  }
                }`;
    let variables = {
      password: this.state.password,
      input: {
        birthdate: "",
        language: "",
        firstname: this.state.firstName,
        lastname: this.state.lastName,
        address: {
          street: this.state.street,
          country: this.state.country,
          city: "",
          state: ""
        }
      }
    };
    if(this.state.password !== ""){
      this.authorizeToken();
      apolloFetch({query,variables}).then((response) => {
        console.log(response);
        if (!response["errors"]) {
          message.success("Successfully updated Personal Information!");
          setTimeout(() => {
            history.push("/accountCenter");
          },800)
        } else {
          console.log(response);
          message.error(response.errors[0].message);
        }
      })
    }else{
      message.error("Please enter your password!");
    }
  };
  keypress = (e) => {
    if (e.which !== 13) return
    this.updatePersonalInformation();
  };
  render() {
    return (
      <div className="personal" >
        <SubComponentHeader/>
        <div className="securityPersonalContent">
          <div className="totalDtc">
            <h3>{this.state.totalDtc} DTC</h3>
            <a>DreamTech Cash Wallet</a>
          </div>
          <div style={{width:"75%",margin:"30px auto"}}>
            <h4>Personal Information</h4>
            <div className="editable">
              <h5>First Name</h5>
              <input type="text"
                     placeholder={this.state.firstName}
                     name="firstName"
                     onKeyPress={this.keypress}
                     onKeyUp={this.vauleChange}/>
              <h5>Last Name</h5>
              <input type="text"
                     placeholder={this.state.lastName}
                     name="lastName"
                     onKeyUp={this.vauleChange}
                     onKeyPress={this.keypress}/>
              <h5>Country</h5>
              <input type="text"
                     placeholder={this.state.country}
                     name="country"
                     onKeyUp={this.vauleChange}
                     onKeyPress={this.keypress}/>
              <h5>Street</h5>
              <input type="text"
                     placeholder={this.state.street}
                     name="street"
                     onKeyUp={this.vauleChange}
                     onKeyPress={this.keypress}/>
              <h5>Email</h5>
              <input type="text"
                     placeholder={this.state.email}
                     name="Email"
                     onKeyUp={this.vauleChange}
                     disabled={true}/>
              <h5>Password</h5>
              <input type="password"
                     placeholder={"Please enter your password!"}
                     name="password"
                     onKeyPress={this.keypress}
                     onKeyUp={this.vauleChange}/>
            </div>
            <span className="update" onClick={this.updatePersonalInformation}>UPDATE</span>
            <span className="back"  onClick={this.backToMyAccount}>BACK</span>
          </div>
        </div>
        <SubComponentFooter/>
      </div>
    );
  }
}

export default PersonalInformation;
