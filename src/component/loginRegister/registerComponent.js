import React, {Component} from 'react';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import Button from 'antd/lib/button';
import Checkbox from 'antd/lib/checkbox';
import {createApolloFetch} from "apollo-fetch";
import message from 'antd/lib/message';
import history from "../../js/history";
import Config from "../../config/config";

/*登陆和注册的模块*/
class RegisterComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: "",
      errorMsg: "",
      firstName: "",
      lastName: "",
      emailAddress: "",
      password: "",
      confirmPassword: ""
    }
  }
  componentDidMount() {
    setTimeout(() => {
      this.input.focus();
    });
  };
  checkInput = (obj) => {
    for (let item in obj) {
      if (obj[item].trim()==="") {
        this.setState({
          errorMsg: "Please enter your " + item + "."
        });
        return false
      }
    }
    return true
  };
  /*注册*/
  handleRegisterSubmit = (e) => {
    e.preventDefault();
    let userInfo = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      emailAddress: this.state.emailAddress,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword,
    };
    if (this.checkInput(userInfo)) {
      if (userInfo.password === userInfo.confirmPassword) {
        if (this.state.checked) {
          this.sendRegisterInfo(userInfo);
        } else {
          this.setState({
            errorMsg: "Please check our Terms and Conditions!"
          });
          return
        }
      } else {
        this.setState({
          errorMsg: "The passwords are inconsistent twice, please re-enter"
        });
        return
      }
    }
  };
  sendRegisterInfo = (userInfo) => {
    let uri = Config.api;
    let apolloFetch = createApolloFetch({uri});
    let query = `mutation ($input:register!) {
                     Register( input:$input) {
                      errors{
                      Path,Message
                      }
                    }
                  }`;
    let variables = {
      input: {
        password: userInfo.password,
        username: userInfo.emailAddress,
        firstname: userInfo.firstName,
        lastname: userInfo.lastName
      }
    };
    apolloFetch({query, variables}).then((response) => {
      let Register = response["Register"].errors;
      if (Register===null) {
        localStorage.setItem("userEmail",userInfo.emailAddress);
        history.push("/verify");
      } else {
        message.error(Register[0]["Path"] +" " + Register[0]["Message"]);
      }
    });
  };
  vauleChange = (e) => {
    this.setState({
      errorMsg: "",
      [e.target.name]: e.target.value
    });
  };
  goBack=()=>{
    history.goBack();
  };
  onChange = (e) => {
    // console.log(`checked = ${e.target.checked}`);
    this.setState({
      checked: e.target.checked,
      errorMsg: ""
    });
  };
  render() {
    return (
      <div>
        <Form onSubmit={this.handleRegisterSubmit}
              className="login-form"
              ref="getSwordButton"
              autoComplete="OFF">
          <Input type="text"
                 ref={node => this.input = node}
                 placeholder="first name"
                 name="firstName"
                 onChange={this.vauleChange}
                 autoFocus="autofocus"/>
          <Input type="text"
                 placeholder="last name"
                 name="lastName"
                 onChange={this.vauleChange}/>
          <Input type="email"
                 placeholder="your email"
                 name="emailAddress"
                 onChange={this.vauleChange}/>
          <Input type="password"
                 placeholder="password"
                 name="password"
                 onChange={this.vauleChange}/>
          <Input type="password"
                 placeholder="confirm password"
                 name="confirmPassword"
                 onChange={this.vauleChange}/>
          <span className="errorMsg">{this.state.errorMsg}</span>
          <Checkbox onChange={this.onChange}>
            <p>
              I have read and agree with <br/>
              <a className="yellowTxt">Terms and Conditions <br/></a>
              and confirm that I am NOT a US citizen
            </p>
          </Checkbox>
          <Button type="primary"
                  htmlType="submit"
                  className="login-form-button">SIGN UP</Button>
          <p>Already have an account? <a className="yellowTxt" onClick={()=>history.push(`/loginregister/login`)} style={{textDecoration:"underline"}}> Log in</a></p>
          <a className="yellowTxt" onClick={()=>history.push(`/`)}>Back to site</a>
        </Form>
      </div>
    )
  }
}

const WrappedNormalLoginForm = Form.create()(RegisterComponent);
export default WrappedNormalLoginForm;
