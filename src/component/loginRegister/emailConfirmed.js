import React, {Component} from 'react';
import history from '../../js/history';
import "../../css/loginRegister/waitForVerifyEmail.css";
import Config from "../../config/config";
import {createApolloFetch} from "apollo-fetch";
import message from "antd/lib/message";

/*登陆和注册的模块*/
class EmailConfirmed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      successStatu:false
    }
  }
  componentDidMount() {
    this.sendTokenToBackend();
  }
  /*判断用户是否登陆*/
  sendTokenToBackend=()=>{
    let uri = Config.api;
    let apolloFetch = createApolloFetch({uri});
    let query = `query($token:String!) {
                    confirmEmail(token:$token )
                 }`;
    let variables = {
      token:this.props.match.params["confirm"]
    };
    apolloFetch({query, variables}).then((response) => {
      console.log(response);
      if(response["confirmEmail"]){
        this.setState({
          successStatu: true,
        });
      }else{
        message.error(response.errors[0]["message"]);
      }
    });
  };
  goToLogin=()=>{
    setTimeout(function(){
      history.push(`/loginregister/${"login"}`);
    },300);
  };
  render() {
    const itemShown = this.state.successStatu
      ? <div className="userInformation">
        <h3>Your email was successfully confirmed. <br/> Congratulations !</h3>
        <span onClick={this.goToLogin} className="loginButton">LOGIN</span>
      </div>
      : <div className="userInformation">
        <h3>Your email was failured confirmed. <br/> Please try again .</h3>
      </div>;
    return (
      <div className="waitForVerify">
        <div className="verifyBody displayflexcenter">
          <div className="verifyContent">
            <img src={require("../../images/logoSample2-01.svg")} alt="logo"/>
            {itemShown}
            <h2>Have some questions? Please contact us at <a>Dreamtech</a></h2>
            <p>© 2018 DreamTech. All rights reserved.</p>
            {/*<a>Back site</a>*/}
          </div>
        </div>
      </div>
    )
  }
}

export default EmailConfirmed;
