import React, {Component} from 'react';
import Form from 'antd/lib/form';
import Tabs from 'antd/lib/tabs';
import RegisterComponent from './registerComponent';
import LoginComponent from './loginComponent';
import history from '../../js/history';
import "../../css/loginRegister/loginRegisterComponent.css";

const TabPane = Tabs.TabPane;
/*登陆和注册的模块*/
class LoginRegisterComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showTab: props.match.params.type
    }
  }
  componentWillReceiveProps(nextProps){
    if (nextProps.location.pathname !== this.props.location.pathname) {
      // window.location.reload(true);
      this.setState({
        showTab:nextProps.match.params.type
      });
    }
  };
  callback=(key)=>{
    this.setState({
      showTab:key
    });
    history.push(`/loginregister/${key}`)
  };
  render() {
    let {showTab} = this.state;
    return (
      <div className="modalMask">
        <div className="loginRegisterComponent">
          <div className="loginRegisterContent">
            <img src={require("../../images/logoSample3-01.svg")} className="logoSample3" alt="logo"/>
            <Tabs activeKey={showTab} onChange={this.callback}>
              <TabPane tab="LOGIN" key="login">
                <LoginComponent/>
              </TabPane>
              <TabPane tab="SIGN UP" key="register">
                <RegisterComponent/>
              </TabPane>
            </Tabs>
          </div>
        </div>
      </div>
    )
  }
}

const WrappedNormalLoginForm = Form.create()(LoginRegisterComponent);
export default WrappedNormalLoginForm;