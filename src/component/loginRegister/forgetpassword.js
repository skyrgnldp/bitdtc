import React, {Component} from 'react';
import history from "../../js/history";
import "../../css/loginRegister/waitForVerifyEmail.css";
import message from "antd/lib/message";
import Config from "../../config/config";
import {createApolloFetch} from "apollo-fetch";

/*登陆和注册的模块*/
class ForGetPassword extends Component {
  constructor() {
    super();
    this.state = {
      hasSendEmail:false,
      eamilAddress:""
    }
  }
  onChangeAddress = (e) => {
    this.setState({eamilAddress: e.target.value});
  };
  handleEnter=(e)=>{
    if (e.which !== 13) return;
    this.sendEmail();
  };
  goToLogin=()=>{
    setTimeout(function(){
      history.push(`/loginregister/${"login"}`);
    },300);
  };
  sendEmail=()=>{
    let pattern = /\w[-\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\.)+[A-Za-z]{2,14}/;
    if(pattern.test(this.state.eamilAddress)){
      let uri = Config.api;
      let that=this;
      let apolloFetch = createApolloFetch({ uri });
      let query = `query($email:String!) { ResetPassword( email:$email) { errors {Path,Message} } }`;
      let variables = {
        email: this.state.eamilAddress,
      };
      apolloFetch({query, variables}).then(function (response) {
        console.log(response);
        if (!response["errors"]) {
          that.setState({
            hasSendEmail: true
          });
        } else {
          message.error(response.errors[0].message);
        }
      })
    }else {
      message.error("Invalid Input");
    }
  };
  render() {
    const itemShown = this.state.hasSendEmail
      ? <div className="userInformation">
          <br/>
          <h3>We have sent a new password to this email <br/>"{this.state.eamilAddress}"</h3>
          <h4>For temporarily,you can use this as your password.<br/>Please change it once you have logged in.</h4>
          <span onClick={this.goToLogin} className="loginButton">Go To Login</span>
        </div>
      : <div className="userInformation">
          <h3>Please enter your Email address <br/>To reset your password.</h3>
          <input type="email" placeholder="Email" onKeyUp={this.onChangeAddress} onKeyPress={this.handleEnter}/>
          <span onClick={this.sendEmail} className="loginButton">Send Email</span>
        </div>;
    return (
      <div className="waitForVerify">
        <div className="verifyBody displayflexcenter">
          <div className="verifyContent">
            <img src={require("../../images/logoSample2-01.svg")} alt="logo"/>
            {itemShown}
            <h2>Have some questions? Please contact us at <a>Dreamtech</a></h2>
            <a className="yellowTxt" onClick={()=>history.push(`/`)}>Back to site</a>
            <p>© 2018 DreamTech. All rights reserved.</p>
          </div>
        </div>
      </div>
    )
  }
}

export default ForGetPassword;
