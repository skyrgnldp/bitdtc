import React, {Component} from 'react';
import "../../css/loginRegister/waitForVerifyEmail.css";
import history from "../../js/history";

/*登陆和注册的模块*/
class WaitForVerify extends Component {
  componentDidMount(){
    setTimeout(function(){
      history.replace("/");
    },10000);
  }

  goToHomePage = () => {
    history.push("/");
  }

  render() {
    return (
      <div className="waitForVerify">
        <div className="verifyBody displayflexcenter">
          <div className="verifyContent">
            <img src={require("../../images/logoSample2-01.svg")} alt="logo"/>
            <div className="userInformation">
              <h3>We have sent an email to <br/> {localStorage.getItem("userEmail")}</h3>
              <h4>Please follow the link in email to <br/> activate your account.</h4>
            </div>
            {/* <div className="displayflexcenter ">
              <a onClick={this.goToHomePage}></a>
            </div> */}
            <h6>Redirecting to Home page. Please wait ...</h6>
            <h2>Have some questions? Please contact us at <a>Dreamtech</a></h2>
            <p>© 2018 DreamTech. All rights reserved.</p>
          </div>
        </div>
      </div>
    )
  }
}

export default WaitForVerify;
