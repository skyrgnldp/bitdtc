import React, {Component} from 'react';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import Button from 'antd/lib/button';
import Config from "../../config/config";
import history from '../../js/history';
import {createApolloFetch} from "apollo-fetch";
import message from "antd/lib/message";
import jwt from "jsonwebtoken";


/*登陆和注册的模块*/
let uri = Config.api;
let apolloFetch = createApolloFetch({uri});

class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMsg: "",
      emailAddress: "",
      password: ""
    }
  }
  componentDidMount() {
    setTimeout(() => {
      this.emailInput.focus();
    });
  };
  checkInput = (obj) => {
    for (let item in obj) {
      if (obj[item].trim()==="") {
        this.setState({
          errorMsg: item + " cannot be empty."
        });
        return false
      }
    }
    return true
  };
  goBack=()=>{
    history.goBack();
  };
  vauleChange = (e) => {
    this.setState({
      errorMsg: "",
      [e.target.name]: e.target.value
    });
  };
  /*先获得输入框的值，再进行判断不为空，然后在传到后台进行验证*/
  handleLoginSubmit = (e) => {
    e.preventDefault();
    let userInfo = {
      emailAddress: this.state.emailAddress,
      password: this.state.password
    };
    let checkResult = this.checkInput(userInfo);
    if (checkResult) {
      let query = `mutation($input:Credentials!) {
                      login( input:$input ) {
                        token,exp,data {
                          Firstname,Lastname,id,Email,Language
                        }
                      }
                    }`;
      let variables = {
        input: {
          username: userInfo.emailAddress,
          password: userInfo.password
        }
      };
      apolloFetch({query, variables}).then((response) => {
        if (response["login"]) {
          let decoded = jwt.decode(response["login"]['token'], {complete: true});
          let login = response["login"];
          localStorage.setItem('token', login['token']);
          localStorage.setItem('exp', login['exp']);
          for (let item in login["data"]) {
            localStorage.setItem(item, login["data"][item])
          }
          if(decoded["payload"]["passwordStatus"]){
            message.success('Login Successful.');
            setTimeout(function () {
              history.replace(`/`);
              window.history.replaceState(null, null);
            },2000);
          }else{
            message.success('Login Successful.');
            setTimeout(function () {
              history.push(`/security`);
            },2000);
          }
        } else {
          message.error(response.errors[0]["message"]);
        }
      });
    }
  };
  render() {
    return (
      <Form onSubmit={this.handleLoginSubmit}
            className="login-form"
            ref="getSwordButton"
            autoComplete="OFF">
        <ul className="signInWith">
          <li className="displayflexbetween">
            <img src={require("../../images/GooglePlus.png")} alt="file"/>
            <a href="https://api.bitdtc.com/auth/google" target="_blank" rel="noopener noreferrer">SIGN IN WITH GOOGLE</a>
          </li>
          {/*<li className="displayflexbetween">*/}
          {/*<img src={require("../../images/facebook.png")} alt="file"/>*/}
          {/*<a>SIGN IN WITH FACEBOOK</a>*/}
          {/*</li>*/}
        </ul>
        <a className="yellowTxt">OR</a>
        <Input type="email"
               ref={node => this.emailInput = node}
               placeholder="youremail@example.com"
               autoFocus="autofocus"
               name="emailAddress"
               onChange={this.vauleChange}/>
        <Input type="password"
               placeholder="your password"
               name="password"
               onChange={this.vauleChange}/>
        <span className="errorMsg">{this.state.errorMsg}</span>
        <a className="yellowTxt" onClick={()=>history.push(`/forgetpassword`)}>Don’t remember your password?</a>
        <Button type="primary"
                htmlType="submit"
                className="login-form-button">LOGIN</Button>
        <a className="yellowTxt" onClick={()=>history.push(`/loginregister/register`)} style={{textDecoration:"underline"}}>Not registered?<br/></a>
        <a className="yellowTxt" onClick={()=>history.push(`/`)}>Back to site</a>
      </Form>
    )
  }
}

export default LoginComponent;
