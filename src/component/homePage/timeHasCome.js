import React, {Component} from 'react';
import "../../css/homePage/timeHasCome.css";
import history from "../../js/history";

const logoList = [
  {img: require("../../images/yobit.png"), name: "yobit"},
  {img: require("../../images/2108.svg"), name: "Livecoin"},
  {img: require("../../images/logo.svg"), name: "aeternity"},
  {img: require("../../images/idex-01.svg"), name: "exchange"}
];

class TimeHasCome extends Component {
  /*判断用户是否登陆*/
  gotoBuyDtc = () => {
    let email = localStorage["Email"];
    if (email) {
      history.push(`/accountCenter`);
    }else{
      history.push(`/loginregister/${"login"}`);
    }
  };
  render() {
    return (
      <div className="timeHasCome">
        <h1>THE TIME HAS COME!</h1>
        <h2>EARN FROM YOUR SKILLS !</h2>
        <h4>The dedicated cryptocurrency for Gamers</h4>
        <div className="DTCTotalSupply">
          <h4>DTC Total Supply:</h4>
          <h5>778418516.077</h5>
          <span className="buyDtcNow" onClick={this.gotoBuyDtc}>BUY DTC NOW</span>
          <h6>You can buy DTC on:</h6>
          <div className="displayflexcenter">
            {
              logoList.map((item, index) => {
                return (
                  <img src={item.img} alt={item.name} key={index}/>
                )
              })
            }
          </div>
        </div>
      </div>
    );
  }
}

export default TimeHasCome;
