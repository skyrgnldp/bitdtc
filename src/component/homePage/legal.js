import React, {Component} from 'react';
import WOW from "wow.js";
import "../../css/homePage/legal.css";

class Legal extends Component {
  componentWillMount() {
    const wow = new WOW(
      {
        boxClass: 'wow',      // default
        animateClass: 'animated', // default
        offset: 100,          // default
        mobile: true,       // default
        live: true        // default
      }
    );
    wow.init();
  }

  render() {
    return (
      <div className="legal">
        <div className="legalContent">
          <h3 className="mainTitle">LEGAL</h3>
          <p className="secondTitle">Join our thriving Telegram and Discord Communities!</p>
          <ul className="displayflexcenter">
            <li className="displayflexcenter wow fadeInLeft">
              <img src={require("../../images/chklst-01.svg")} alt="file"/>
              <a href="https://file.bitdtc.com/lightpaper.pdf" target="_blank" rel="noopener noreferrer">LIGHT PAPER</a>
            </li>
            <li className="displayflexcenter wow fadeInRight">
              <img src={require("../../images/chklst-01.svg")} alt="file"/>
              <a href="https://file.bitdtc.com/whitepaper.pdf" target="_blank" rel="noopener noreferrer">WHITE PAPER</a>
            </li>
            {/* <li className="displayflexcenter">
              <img src={require("../../images/chklst-01.svg")} alt="file"/>
              <a>TERMS & CONDITIONS</a>
            </li> */}
          </ul>
        </div>
      </div>
    );
  }
}

export default Legal;
