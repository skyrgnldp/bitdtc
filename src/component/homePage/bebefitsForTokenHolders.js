import React, {Component} from 'react';
import WOW from "wow.js";
import "../../css/homePage/bebefitsForTokenHolders.css";

const tokenDetails = [
  {name: "Name", details: "DreamTechCash"},
  {name: "Symbol", details: "DTC"},
  {name: "Token Sale Date", details: "June 27th 18:00 UTC"},
  {name: "Token Sale End", details: "January 12th 19:00 UTC"},
  {name: "Hidden CAP", details: "Revealed if 70% of the cap is reached"},
  {name: "Currencies accepted", details: "BTC, ETH, BCH, LTC, DASH"},
  {name: "Bonus for all", details: "120 hours +15%. December 21st, 2017 - January 5th, 2018: 12%. Starting January 6th,2018 the bonus amount will decrease 1% every 24 hours until 18:59 UTC on January 31,2018"},
  {name: "Bonus for big investors", details: "+20% for 200+ ETH transactions (throughout token sale)"},
  {name: "Token Standard", details: "ERC-20"},
  {name: "Token Features", details: "Utility token"},
  {name: "Token exchange rate ", details: "0.001 ETH per 1 CRC (i.e. 1000 CRC per 1 ETH)"},
  {name: "Planed listing on exchanges ", details: "1-15 February 2018"}
];
const holders = [
  "Safe and convenient way to make an income from gaming and game development.",
  "Pay for in-game assets, items and services.",
  "Use CRYCASH SDK to integrate CRYCASH with your products and reach millions of players.",
  "A viable cryptocurrency thanks to the Buy Back program."
];

class BebefitsForTokenHolders extends Component {
  componentWillMount() {
    const wow = new WOW({
        boxClass: 'wow',      // default
        animateClass: 'animated', // default
        offset: 100,          // default
        mobile: true,       // default
        live: true        // default
      });
    wow.init();
  }

  render() {
    let playStyle = {
      width:"1024px",
      height:"auto"
    };
    return (
      <div className="bebefits">
        <h3 className="mainTitle">BENEFITS FOR TOKEN HOLDERS</h3>
        <ul className="holders">
          {
            holders.map((item, index) => {
              return (
                <li key={index}><span><em> </em></span>{item}</li>
              )
            })
          }
        </ul>
        <h3 className="mainTitle">TOKEN DETAILS</h3>
        <ul className="tokenDetails">
          {
            tokenDetails.map((item, index) => {
              return (
                <li key={index}><span>=</span><em>{item.name}</em><i>{item.details}</i></li>
              )
            })
          }
        </ul>
        <h3 className="mainTitle">TOKEN SALE STRUCTURE</h3>
        <img style={playStyle} src={require("../../images/dtc-flowCash.svg")} alt="flowCash"/>
        <h3 className="mainTitle">SO WHO GETS WHAT?</h3>
        <ul className="displayflexcenter">
          <li>
            <span>Token Holders</span>
            <p>All token holders are guaranteed a <br/>general reward out of all distributed prize<br/>pools.</p>
          </li>
          <li>
            <span>Players</span>
            <p>Game winner receive their respective<br/>part of the prize.</p>
          </li>
          <li>
            <span>Game Creators</span>
            <p>Game creators are rewarded for their<br/>effort to create and manage games.</p>
          </li>
        </ul>
      </div>
    );
  }
}

export default BebefitsForTokenHolders;
