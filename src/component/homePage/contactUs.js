import React, {Component} from 'react';
import Form from 'antd/lib/form';
import Input from 'antd/lib/input';
import Button from 'antd/lib/button';
import message from "antd/lib/message";
import PopUpModal from "../popupwindow/popupwindow";
import {createApolloFetch} from "apollo-fetch";
import Config from "../../config/config";
import '../../css/homePage/contactUs.css';

const {TextArea} = Input;
const FormItem = Form.Item;
const contactList = [
  {index: "1", image: require("../../images/LocationIcon.svg"), title: "OFFICE", content: "Makati,1230 Metro Manila"},
  {index: "2", image: require("../../images/FBIcon.svg"), title: "FACEBOOK", content: "@vpncashorg"},
  {index: "3", image: require("../../images/PhoneIcon.svg"), title: "PHONE", content: "+41 214 5124"},
  {index: "4", image: require("../../images/TwitterIcon.svg"), title: "TWITTER", content: "@vpncashorg"},
  {index: "5", image: require("../../images/EmailIcon.svg"), title: "EMAIL", content: "info@vpncash.org"},
  {index: "6", image: require("../../images/TelegramIcon.svg"), title: "TELEGRAM", content: "@vpncashorgdaily"}
];

class ContactUs extends Component {
  constructor() {
    super();
    this.state = {
      contactInfo: {},
      visible:false
    };
  }
  //重置函数
  handleReset = () => {
    this.props.form.resetFields();
  };
  handleSubmit = (e) => {
    e.preventDefault();
    let that = this;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        let reg =/^[1-9]\d*$/;
        if (reg.test(values["mobile"])){
          let uri = Config.api;
          let apolloFetch = createApolloFetch({uri});
          let query = `mutation($input:ContactusInputFields!){
                        SendMessage(input:$input){
                          message,errors{Path,Message}
                         }
                       }`;
          let variables = {
            input:{
              fullname:values.name,
              email:values.email,
              mobilenumber:{
                countrycode:"PH",
                number:values.mobile
              },
              subject:"there is no subject",
              message:values.message
            }
          };
          apolloFetch({query, variables}).then((response) => {
            if (response["SendMessage"].message) {
              that.handleReset();
              this.child.showModal();
            } else {
              message.error(response["SendMessage"].errors[0]["Message"]);
            }
          })
        } else {
          message.error('Input a valid mobile number!');
          return
        }
      }
    });
  };
  onRef = (ref) => {
    this.child = ref
  };
  render() {
    let {getFieldDecorator} = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="contactUs" autoComplete="OFF">
        <div className="contactContent">
          <h3>CONTACT US</h3>
          <div className="displayflexbetween" style={{alignItems: "flex-start"}}>
            <div>
              <FormItem>
                {getFieldDecorator('name', {
                  rules: [{required: true, message: 'Please input your name!'}],
                })(<Input placeholder="Full Name"/>)}
              </FormItem>
              <FormItem>
                {getFieldDecorator('email', {
                  rules: [{required: true, message: 'Please input your email!'}],
                })(<Input placeholder="Email" type="email"/>)}
              </FormItem>
              <FormItem>
                {getFieldDecorator('mobile', {
                  rules: [{required: true, message: 'Please input a valid mobile number!'}],
                })(<Input placeholder="Mobile Number" type="text"/>)}
              </FormItem>
              <FormItem id="message">
                {getFieldDecorator('message', {
                  rules: [{required: true, message: 'Send us your feedback .'}],
                })(<TextArea placeholder="Message" className="txtAreaNoResize"/>)}
              </FormItem>
              <Button type="primary" htmlType="submit">Submit</Button>
            </div>
            <div className="contactIcons">
              <p className="contactsMainText">Feel Free to Drop Us a Line</p>
              <ul className="displayflexend">
                {contactList.map((item, index) => (
                  <li key={index}>
                    <img src={item.image} alt="left"/>
                    <span>{item.title}</span>
                    <p>{item.content}</p>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>
        {/* Map */}
        <img src={require("../../images/dottedmap.svg")} className="map" alt="left"/>
        <PopUpModal onRef={this.onRef}/>
      </Form>
    );
  }
}

const WrappedApp = Form.create()(ContactUs);
export default WrappedApp;
