import React, {Component} from 'react';
// import {Player} from 'video-react';
import "../../css/homePage/videoRoadMap.css";
import "../../../node_modules/video-react/dist/video-react.css";

const timer = [
  {now: 0, year: 2018, month: "March", text: "Token Sale launch"},
  {now: 1, year: 2018, month: "April", text: "Casino software development"},
  {now: 2, year: 2018, month: "May", text: "Exchange listings announcement"},
  {now: 3, year: 2018, month: "June", text: "Forming DreamTech ICO development team"},
  {now: 4, year: 2018, month: "July", text: "Market analysis and project planning"},
  {now: 5, year: 2018, month: "August", text: "Crowd sale"},
  {now: 6, year: 2018, month: "September", text: "End of crowd sale"},
  {now: 7, year: 2018, month: "October", text: "Development of user nodes"},
  {now: 8, year: 2018, month: "November", text: "Creation of centralized server"},
  {now: 9, year: 2018, month: "December", text: "Initial phase of game development"},

  {now: 10, year: 2019, month: "January", text: "Initial layer of security protocols"},
  {now: 11, year: 2019, month: "February", text: "Payment development"},
  {now: 12, year: 2019, month: "March", text: "Smart Contract development"},
  {now: 13, year: 2019, month: "April", text: "Game development"},
  {now: 14, year: 2019, month: "May", text: "Betting platform development"},
  {now: 15, year: 2019, month: "June", text: "Alpha version release"},
  {now: 16, year: 2019, month: "July", text: "Building community"},
  {now: 17, year: 2019, month: "August", text: "Central server version II"},
  {now: 18, year: 2019, month: "September", text: "Nodes and module optimization"},
  {now: 19, year: 2019, month: "October", text: "Additional security protocols"},
  {now: 20, year: 2019, month: "November", text: "Decentralization"},
  {now: 21, year: 2019, month: "December", text: "Testing"},

  {now: 22, year: 2020, month: "January", text: "Deployment"},
  {now: 23, year: 2020, month: "February", text: "Coming soon..."},
  {now: 24, year: 2020, month: "March", text: "Coming soon..."},
  {now: 25, year: 2020, month: "April", text: "Coming soon..."},
  {now: 26, year: 2020, month: "May", text: "Coming soon..."},
  {now: 27, year: 2020, month: "June", text: "Coming soon..."}
];

class VideoRoadMap extends Component {
  constructor() {
    super();
    this.state = {
      default: 4,
      now: 4,
      total: 20
    }
  };
  /*create time line for road map*/
  createTimeLine=()=>{
    let tools = [];
    for(let i= 0;i < 42;i++){
      if(i === 7 || i === 14 || i=== 28 || i=== 35){
        tools.push(<span className="longLine" key={i} > </span>)
      }else if(-1 < i < 7 || 7 < i < 14 || 14 < i <28 || 28 < i < 35 || 35 < i < 42 ){
        tools.push(<span key={i} > </span>)
      }else if(14 < i < 28){
        tools.push(<span className="borderNone" key={i} > </span>)
      }
    }
    return tools;
  };
  /*addTime */
  addTime = () => {
    if (this.state.now < this.state.total) {
      this.setState({
        now: this.state.now + 1
      })
    } else {
      this.setState({
        now: this.state.now
      })
    }
  };
  /*decreaseTime */
  decreaseTime = () => {
    if (this.state.now > 1) {
      this.setState({
        now: this.state.now - 1
      })
    } else {
      this.setState({
        now: this.state.now
      })
    }
  };
  /*reset to default time*/
  setNowToDefault = () => {
    this.setState({
      now: this.state.default
    })
  };

  render() {
    return (
      <div className="videoRoadMap">
        <div className="blackcover">
          {/* <Player fluid={false} width={640} height={420} loop
                  poster="http://www.dtshow.vip/img/item01.jpg"><source src="https://www.youtube.com/embed/GxaA7ukbQKQ" /></Player> */}

          <iframe width="640" height="420" src="https://www.youtube.com/embed/GxaA7ukbQKQ" frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen></iframe>
          <p className="secondTitle">
            The Dream Tech ecosystem consists of game creators and players. Everyone
            is able to participate by simply joining or creating games. <br/>
            The whole community benefits from a growing activity as every DT Cash
            holder is ensured to receive a general reward of all played games.<br/>
            Additionally, those who take the effort to create games receive a provider reward.
          </p>
          <h3 className="mainTitle">ROAD MAP</h3>
          <div className="roadmap">
            <div className="displayflexbetween year">
              <span>{timer[this.state.now + 2]["year"] - 1}</span>
              <span className="active">{timer[this.state.now + 2]["year"]}</span>
              <span>{timer[this.state.now + 2]["year"] + 1}</span>
            </div>
            <div className="displayflexbetween month">
              <span>{timer[this.state.now - 1]["month"]}</span>
              <span>{timer[this.state.now]["month"]}</span>
              <span>{timer[this.state.now + 1]["month"]}</span>
              <span className="active">{timer[this.state.now + 2]["month"]}</span>
              <span>{timer[this.state.now + 3]["month"]}</span>
              <span>{timer[this.state.now + 4]["month"]}</span>
              <span>{timer[this.state.now + 5]["month"]}</span>
            </div>
            <div className="displayflexbetween line">
              {this.createTimeLine()}
            </div>
            <div className="displayflexbetween roadmapFooter">
              <div className="displayflexstart">
                <em className="now" onClick={this.setNowToDefault.bind(this)}>Now</em>
              </div>
              <div className="mesagers">
                <i className="arrowUp"> </i>
                <h4>{timer[this.state.now + 2]["text"]}</h4>
              </div>
              <div className="displayflexend buttons">
                <em> <b style={{color: "#ffc30e"}}> {this.state.now} </b> of {this.state.total} </em>
                <em className="fa fa-angle-left" onClick={this.decreaseTime.bind(this)}> </em>
                <em className="fa fa-angle-right" onClick={this.addTime.bind(this)}> </em>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default VideoRoadMap;
