import React, {Component} from 'react';
import ComponentHeader from "../componentHeader";
import TimeHasCome from "../../component/homePage/timeHasCome";
import Competewinearn from "../../component/homePage/competewinearn";
import DreamTechCash from "../../component/homePage/dreamTechCash";
import LetsChat from "../../component/homePage/letsChat";
import VideoRoadMap from "../../component/homePage/videoRoadMap";
import BebefitsForTokenHolders from "../../component/homePage/bebefitsForTokenHolders";
import Legal from "../../component/homePage/legal";
import TheInsideScoop from "../../component/homePage/theInsideScoop";
import ComponentFooter from "../../component/componentFooter";
import JoinUs from "../../component/homePage/joinus";
import ContactUs from "../../component/homePage/contactUs";
import message from "antd/lib/message";
import BackTop from 'antd/lib/back-top';
import WOW from "wow.js";
import {Player} from "video-react";

message.config({
  top: 50,
  duration: 2,
  maxCount: 1,
});

class App extends Component {
  constructor() {
    super();
    this.state = {

    };
  }
  componentWillMount() {
    const wow = new WOW({
      boxClass: 'wow',      // default
      animateClass: 'animated', // default
      offset: 100,          // default
      mobile: true,       // default
      live: true        // default
    });
    wow.init();
  }
  render() {
    let playStyle = {
      width:"100vw",
      minWidth:"1024px",
      height:"auto"
    };
    return (
      <div className="app">
        <div className="mainContent">
          <Player style={playStyle}
                  loop muted autoPlay={true}
                  src={require("../../images/DreamTech-VidBanner2v3.mp4")}/>
          <div className="disableFullScreen">
            <ComponentHeader />
            <TimeHasCome/>
          </div>
        </div>
        <Competewinearn/>
        <DreamTechCash/>
        <LetsChat/>
        <VideoRoadMap/>
        <BebefitsForTokenHolders/>
        <Legal/>
        <TheInsideScoop/>
        <JoinUs/>
        <ContactUs />
        <ComponentFooter/>
        <BackTop visibilityHeight="400" id="backTup">
          <span className="fas fa-angle-up"> </span>
        </BackTop>
      </div>
    );
  }
}

export default App;
