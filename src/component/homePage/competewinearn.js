import React, {Component} from 'react';
import WOW from "wow.js";
import "../../css/homePage/competewinearn.css";

class App extends Component {
  componentWillMount() {
    const wow = new WOW({
      boxClass: 'wow',// default
      animateClass: 'animated', // default
      offset: 100,// default
      mobile: true,// default
      live: true // default
    });
    wow.init();
  }

  render() {
    return (
      <div className="competewinearn">
        <ul className="displayflexbetween">
          <li className="compete wow fadeInLeft"
              data-wow-duration="0.9s"
              data-wow-delay=".1s">
            <span>
              <img src={require("../../images/fist-01.svg")} alt="compete"/>
            </span>
            <h5>COMPETE</h5>
            <p>
              <em>Sign up</em>, Connect and compete <br/> against players of all skills.</p>
          </li>
          <li className="win">
            <span>
              <img src={require("../../images/trophy-01.svg")} alt="win"/>
            </span>
            <h5>WIN</h5>
            <p>Solo or with <br/> a Team a <em>Win</em> is a <em>Win </em> !</p>
          </li>
          <li className="earn wow fadeInRight" data-wow-duration="0.9s" data-wow-delay=".1s">
            <span>
              <img src={require("../../images/diamond-01.svg")} alt="earn"/>
            </span>
            <h5>EARN</h5>
            <p>Get paid for your skill <br/> in no time.</p>
          </li>
        </ul>
      </div>
    );
  }
}

export default App;
