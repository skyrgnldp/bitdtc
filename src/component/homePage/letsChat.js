import React, {Component} from 'react';
import WOW from "wow.js";
import "../../css/homePage/letsChat.css";

class LetsChat extends Component {
  componentWillMount() {
    const wow = new WOW(
      {
        boxClass: 'wow',      // default
        animateClass: 'animated', // default
        offset: 500,          // default
        mobile: true,       // default
        live: true        // default
      }
    );
    wow.init();
  }
  render() {
    return (
      <div className="letschat">
        <h3 className="mainTitle">LET’S CHAT</h3>
        <p className="secondTitle">Join our thriving Telegram and Discord Communities!</p>
        <div className="displayflexcenter">
          <a className="telegram displayflexbetween wow fadeInLeft" href="https://t.me/bitdtc" target="_blank" rel="noopener noreferrer">
            <img src={require("../../images/telegram-01.svg")} alt="telegram"/>
            <em>TELEGRAM</em>
          </a>
          <a className="discord displayflexbetween wow fadeInRight" href="https://discord.gg/jMyDt5B" target="_blank" rel="noopener noreferrer">
            <img src={require("../../images/discord-01.svg")} alt="discord"/>
            <em>DISCORD</em>
          </a>
        </div>
      </div>
    );
  }
}

export default LetsChat;
