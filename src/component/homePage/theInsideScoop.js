import React, {Component} from 'react';
import NewsComponent from '../news/newsComponent';
import {createApolloFetch} from "apollo-fetch";
import message from "antd/lib/message";
import "../../css/homePage/theInsideScoop.css";
import Config from "../../config/config";

class TheInsideScoop extends Component {
  constructor(){
    super();
    this.state = {
      projectItems : []
    }
  };
  componentDidMount() {
    this.fecthNewsData();
  }
  fecthNewsData = () => {
    let uri = Config.api;
    let query = `query($limit:Limit,$sort:sort,$search:search){
                    News(limit:$limit,sort:$sort,search:$search){
                      data {
                        id,author,headline,headlineimage,content,status,tags,publishdate,category
                      }
                      page {
                        items,pageNo,totalPages
                      } sortableFields,searchableFields
                    }
                 }`;
    let variables = {
      limit:{
        to:3,
        page:1
      },
      sort:{
        field:"category",
        by:"asc"
      },
      search:{
        by:"",
        text:""
      }
    };

    let apolloFetch = createApolloFetch({uri});
    apolloFetch({query,variables}).then((response)=>{
      if (response["News"]["data"].length) {
        let allNews = response["News"]["data"];
        this.setState({
          projectItems:allNews
        });
      }else {
        message.error("Did not load any data, please try again");
      }
    });
  };
  render() {
    return (
      <div className="theInsideScoop">
        <div className="theScoopContent" id="insideScoop">
          <h3 className="mainTitle">THE INSIDE SCOOP</h3>
          <p className="secondTitle">Here's what's going on at Bountie.</p>
          <NewsComponent items={this.state.projectItems}
                         current={1}
                         itemscount={3}
                         fontColor={{color:"#fff"}}/>
        </div>
      </div>
    );
  }
}

export default TheInsideScoop;
