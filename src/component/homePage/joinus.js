import React, {Component} from 'react';
import {Form, Input, message, Modal} from 'antd';
import {createApolloFetch} from "apollo-fetch";
import PopUpModal from "../popupwindow/popupwindow";
import Config from '../../config/config';
import "../../css/homePage/joinus.css";

const FormItem = Form.Item;
const logoList = [
  {img: require("../../images/liteCoin-BnW.svg"), name: "liteCoin"},
  {img: require("../../images/digital_journal_transp-BnW.svg"), name: "digital_journal"},
  {img: require("../../images/CryptoCoinsNews-BnW.svg"), name: "CoinsNews"},
  {img: require("../../images/cointelegraph-logo-BnW.svg"), name: "cointelegraph"}
];

class JoinUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
  };
  handleReset = () => {
    this.props.form.resetFields();
  };
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        if (values["subscribeEmail"].trim()) {
          let that = this;
          let uri = Config.api;
          let apolloFetch = createApolloFetch({uri});
          let query = `mutation($email:String!){suscribe(email:$email){ success,errors{Path,Message}}}`;
          let variables = {
            email: values["subscribeEmail"],
          };
          apolloFetch({query, variables}).then((response) => {
            if (!response["errors"]) {
              this.showModal();
              that.handleReset();
            } else {
              message.error(response.errors[0].message);
            }
          })
        } else {
          message.error('Email address cannot be empty！');
        }
      }
    });
  };
  showModal = () => {
    this.setState({
      visible: true
    });
  };
  handleCancel = (e) => {
    this.setState({
      visible: false
    });
  };
  onRef = (ref) => {
    this.child = ref
  };
  render() {
    const {getFieldDecorator} = this.props.form;
    return (
      <Form className="joinus" onSubmit={this.handleSubmit} autoComplete="OFF">
        <div className="joinusContent" id="joinus">
          <h3 className="mainTitle">JOIN US!</h3>
          <p className="secondTitle">Things are happening real quick at Dream Tech. <br/> Join us in changing the
            gaming
            lifestyle.</p>
          <div className="displayflexcenter" id="asfeaturedOn">
            <FormItem>
              {getFieldDecorator('subscribeEmail', {rules: [{required: false}],})(
                <Input type="email" placeholder="E-Mail address!"  />)}
            </FormItem>
            <button className="signMeUp">Subscribe</button>
          </div>
        </div>
        <div className="asFeatured">
          <div className="asFeaturedContent">
            <h3 className="mainTitle">AS FEATURED ON</h3>
            <div className="displayflexbetween">
              {
                logoList.map((item, index) => {
                  return (
                    <img src={item.img} alt={item.name} key={index}/>
                  )
                })
              }
            </div>
          </div>
        </div>
        <div className="popupWindow">
          <Modal visible={this.state.visible}
                 footer={null}
                 maskClosable={false}
                 onCancel={this.handleCancel}>
            <div className="popupWindowContent">
              <img src={require("../../images/logoSample2-01.svg")} alt="logo"/>
              <div className="userInformation">
                <h3>Thank you <br/>for joining us !</h3>
                {/* <h4>We got your message and will get back to <br/> you soon as we can.</h4> */}
              </div>
              <a>Follow us on:</a>
              <div className="displayflexcenter">
                <img src={require("../../images/followuson/Google+.svg")} alt="facebook"/>
                <img src={require("../../images/followuson/facebook.svg")} alt="facebook"/>
                <img src={require("../../images/followuson/instagram.svg")} alt="facebook"/>
                <img src={require("../../images/followuson/twitter.svg")} alt="facebook"/>
              </div>
              <h2> </h2>
              <p>© 2018 DreamTech. All rights reserved.</p>
            </div>
          </Modal>
        </div>
        <PopUpModal onRef={this.onRef}/>
      </Form>
    );
  }
}

const WrappedNormalLoginForm = Form.create()(JoinUs);
export default WrappedNormalLoginForm;
