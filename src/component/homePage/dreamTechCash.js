import React, {Component} from 'react';
import "../../css/homePage/dreamTechCash.css";

class DreamTechCash extends Component {
  render() {
    return (
      <div className="dreamTechCash">
        <img src={require("../../images/LogoSymbol-01.svg")} alt="LogoSymbol"/>
        <h3 className="mainTitle">DREAM TECH CASH</h3>
        <p className="secondTitle">
          Dream Tech has a regular gambling license, and all series of games are certified by professional algorithms,
          based on credibility and brand reputation. <br/>
          Since its establishment in 2016, the company has won numerous awards in just two years, relying on the team's
          fighting spirit. We are highly praised by many players;<br/>
          we are committed to becoming the best online gamer in the world with our strong strength.
        </p>
      </div>
    );
  }
}

export default DreamTechCash;
