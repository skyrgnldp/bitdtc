import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import $ from "jquery";
import history from '../js/history';
import message from "antd/lib/message/index";
import "../css/header.css";
import jwt from "jsonwebtoken";

class SubComponentHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogined: false,
      email:"",
    };
  };
  componentDidMount() {
    this.goToConfirmedPage();
    this.googleLogin();
    this.userLogined();
    setTimeout(function(){$(window).scrollTop(0)},10)
  }

  getQueryString = (name) => {
    let reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    let r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
  };
  goToConfirmedPage = () => {
    let params = this.getQueryString("confirm");
    if (params) {
      history.push(`/confirmed/${params}`);
    }
  };
  googleLogin = () => {
    let params = this.getQueryString("token");
    if (params) {
      const decoded = jwt.decode(params, {complete: true});
      if(decoded["payload"]["passwordStatus"]){
        for (let item in decoded.payload) {
          localStorage.setItem(item, decoded.payload[item]);
        }
        localStorage.setItem("token", params);
      }
      history.push("/");
    }
  };
  /*判断用户是否登陆*/
  userLogined = () => {
    let email = localStorage["Email"];
    if (email) {
      this.setState({
        isLogined: true,
        email: email
      });
      this.interval();
    }
  };
  /*token过期*/
  interval = () => {
    setInterval(() => {
      let expired = localStorage.getItem('exp');
      let unixTimestamp = expired * 1000;
      let datenow = new Date().getTime();
      if (unixTimestamp < datenow) {
        this.handleLogout();
      }
    }, 1000);
  };
  /*登出*/
  handleLogout = () => {
    clearInterval(this.interval);
    this.setState({
      isLogined: false
    });
    localStorage.clear();
    console.clear();
    message.success('Logout Successful!');
    setTimeout(function () {
      history.push("/");
      window.location.reload();
    }, 300);
  };
  render() {
    let loginedHeader = this.state.isLogined
      ? <div className="logoutEmail">
        <a className="logout" onClick={this.handleLogout} style={{textDecoration:"underline"}}>LOGOUT <span className="fa fa-sign-out-alt"> </span></a>
        <a className="useremail">{this.state.email} <span className="fa fa-cog"> </span>
          <ul className="downList">
            <li onClick={()=>{history.push("/personal")}}><em className="fa fa-user-circle"> </em>Account Settings</li>
            <li onClick={()=>{history.push("/security")}}><em className="fa fa-shield-alt"> </em>Security Settings</li>
          </ul>
        </a>
      </div>
      : <div className="right displayflexend">
          <Link className="signin" to={`/loginregister/${"login"}`}>Login</Link>
          <Link className="getStart" to={`/loginregister/${"register"}`}>Sign Up</Link>
      </div>;
    return (
      <div className="subComponentHeader" ref={node => this.nodeTop = node}>
        <div className="subHeaderContent displayflexbetween">
          <div className="left displayflexstart">
            <img className="logo" src={require("../images/LogoSymbol-01.svg")} alt="logo" onClick={()=>{window.location.reload()}}/>
            <Link to={`/`}>DREAMTECH</Link>
            <a className="chatlogo" href="https://t.me/bitdtc" target="_blank" rel="noopener noreferrer">
              <img className="telegram" src={require("../images/telegram-01.svg")} alt="telegram"/>
            </a>
            <a className="chatlogo" href="https://discord.gg/jMyDt5B" target="_blank" rel="noopener noreferrer">
              <img className="discord" src={require("../images/discord-01.svg")} alt="discord"/>
            </a>
          </div>
          {loginedHeader}
        </div>
      </div>
    );
  }
}

export default SubComponentHeader;
