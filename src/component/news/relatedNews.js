import React, {Component} from 'react';
import NewsComponent from './newsComponent';
import SubComponentHeader from '../subComponentHeader';
import SubComponentFooter from '../subComponentFooter';
import Pagination from 'antd/lib/pagination';
import message from "antd/lib/message";
import Config from "../../config/config";
import {createApolloFetch} from "apollo-fetch";
import "../../css/news/news.css";

let uri = Config.api;
let apolloFetch = createApolloFetch({uri});

class NewsDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultgagenum: 9,
      current: 1,
      newsSingleData: {},
      newsSingleImg: "111",
      newsSingleContent: "",
      relatedNews: []
    }
  };

  componentDidMount() {
    this.fetchTagedNews();
  }

  componentWillReceiveProps(newProps) {
    if (newProps.location.pathname !== this.props.location.pathname) {
      this.fetchTagedNews();
      this.node.scrollIntoView();
    }
  }
  fetchTagedNews = () => {
    let TAGS = [];
    TAGS.push(this.props.match.params.tag);
    let query = `query($limit:Limit,$sort:sort,$search:search){
                    News(limit:$limit,sort:$sort,search:$search){ 
                      data { 
                        id,author,headline,headlineimage,content,status,tags,publishdate,category 
                      } 
                      page { 
                        items,pageNo,totalPages 
                      } sortableFields,searchableFields 
                    }
                 }`;
    let variables = {
      limit: {
        to: "10000", page:"1"
      },
      sort: {
        field: "created", by: "asc"
      },
      search: {
        by: "tags", text:JSON.stringify(TAGS)
      }
    };
    apolloFetch({query, variables}).then((response) => {
      debugger
      if (!response["errors"]) {
        let recommenNews = response["News"]["data"];
        this.setState({
          relatedNews: recommenNews
        });
      } else {
        message.error("Did not load any data, please try again");
      }
    })
  };
  onShowSizeChange = (current, pageSize) => {
    //改变每一页显示的条数改变
    this.setState({defaultgagenum: pageSize})
  };
  pageChange = (page, pageSize) => {
    this.setState({
      current: page
    });
    // console.log("当前页码：", page, "每一页显示的条数：", pageSize);
  };
  render() {
    function showTotal(total) {
      //控制总数的显示方式
      return `Total ${total} Items`;
    }

    function itemRender(current, type, originalElement) {
      if (type === 'prev') {
        return <a>Prev</a>;
      } else if (type === 'next') {
        return <a>Next</a>;
      }
      return originalElement;
    }
    return (
      <div ref={node => this.node = node} id="relatedNews">
        <SubComponentHeader/>
        <div className="relatedNews">
          <NewsComponent items={this.state.relatedNews}
                         current={1}
                         itemscount={this.state.relatedNews.length}
                         fontColor={{color: "#000"}}/>
        </div>
        <div className="pageBtns">
          <Pagination defaultPageSize={this.state.defaultgagenum}
                      total={this.state.relatedNews.length}
                      showTotal={showTotal}
                      showQuickJumper
                      hideOnSinglePage={false}
                      itemRender={itemRender}
                      onChange={this.pageChange}
                      onShowSizeChange={this.onShowSizeChange}/>
        </div>
        <SubComponentFooter/>
      </div>
    )
  }
}

export default NewsDetail;
