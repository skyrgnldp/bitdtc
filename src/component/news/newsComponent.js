import React, {Component} from 'react';
import history from "../../js/history";
import Parser from "html-react-parser";
import "../../css/news/news.css";

class NewsImageLayout extends Component {
  goToNewsDetial=(newsId)=>{
    history.push(`/newsdetail/${newsId}`);
  };
  goToTagsNews=(tags)=>{
    history.push(`/relatedNews/${tags}`);
  };
  render() {
    const Items = this.props.items;
    const percount = this.props["itemscount"]; //每一页显示的条数
    const current = this.props.current; //当前页码
    const startitem = current * percount - percount + 1;//每一开始的条数的id
    const enditem = current * percount;//每一结束的条数的id
    const NewItems = Items.slice(startitem - 1, enditem);
    const listItems = NewItems
      ? NewItems.map((newsItem, index) =>
        <li key={index} className="singleNews">
          <a onClick={()=>this.goToNewsDetial(newsItem.id)}>
            <img className="newsImg" src={newsItem["headlineimage"]} alt="newsImage"/>
            <div className="author">{newsItem["author"]}</div>
            <div className="headline" style={this.props.fontColor}>{newsItem["headline"]}</div>
            <div className="content" style={this.props.fontColor}>{Parser(newsItem["content"])}</div>
            <div className="tags">
              TAGS:&nbsp;{newsItem["tags"].map((tag,index)=>
              <span key={index} onClick={()=>this.goToTagsNews(tag)}>{tag}</span>)
            }
            </div>
            <span>Read more ...</span>
          </a>
        </li>
      )
      : "No news updated.";
    return (
      <ul className="newsComponent displayflexbetween">
        {listItems}
      </ul>
    );
  }
}

export default NewsImageLayout;