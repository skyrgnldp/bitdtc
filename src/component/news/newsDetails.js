import React, {Component} from 'react';
import Parser from "html-react-parser";
import NewsComponent from './newsComponent';
import SubComponentHeader from '../subComponentHeader';
import SubComponentFooter from '../subComponentFooter';
import {createApolloFetch} from "apollo-fetch";
import "../../css/news/news.css";
import "../../css/footer.css";
import message from "antd/lib/message";
import Config from "../../config/config";


let uri = Config.api;
let apolloFetch = createApolloFetch({uri});

class NewsDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newsSingleData: {},
      newsSingleImg: "111",
      newsSingleContent: "",
      recommendedNews: []
    }
  };

  componentDidMount() {
    this.fetchNewsThree();
    this.fetchSingleNewsData(this.props.match.params.id);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.location.pathname !== this.props.location.pathname) {
      this.fetchSingleNewsData(newProps.match.params.id);
      this.fetchNewsThree();
      this.node.scrollIntoView();
    }
  }

  /*获得单个新闻详细数据*/
  fetchSingleNewsData = (newsId) => {
    let query = `query($limit:Limit,$sort:sort,$search:search){
                    News(limit:$limit,sort:$sort,search:$search){
                      data {
                        id,author,headline,headlineimage,content,status,tags,publishdate,category
                      }
                      page {
                        items,pageNo,totalPages
                      } sortableFields,searchableFields
                    }
                 }`;
    let variables = {
      limit:{
        to:3, page:1
      },
      sort:{
        field:"created", by:"asc"   /**desc : descending,asc:ascending**/
      },
      search:{
        by:"id", text:newsId
      }
    };
    apolloFetch({ query, variables }).then((response) => {
      if(response["News"]["data"].length){
        let  getNews = response["News"]["data"][0];
        this.setState({
          newsSingleData:getNews ,
          newsSingleImg: getNews["headlineimage"],
          newsSingleContent: getNews["content"]
        });
      }
    })
  };

  fetchNewsThree = () => {
    let query = `query($limit:Limit,$sort:sort,$search:search){
                    News(limit:$limit,sort:$sort,search:$search){
                      data {
                        id,author,headline,headlineimage,content,status,tags,publishdate,category
                      }
                      page {
                        items,pageNo,totalPages
                      } sortableFields,searchableFields
                    }
                 }`;
    let variables = {
      limit:{
        to:3,
        page:1
      },
      sort:{
        field:"category",
        by:"asc"
      },
      search:{
        by:"",
        text:""
      }
    };
    apolloFetch({ query,variables}).then((response) => {
      if (response["News"]["data"].length) {
        let recommenNews = response["News"]["data"];
        this.setState({
          recommendedNews:recommenNews
        });
      }else {
        message.error("Did not load any data, please try again");
      }
    })
  };

  render() {
    let {newsSingleData} = this.state;
    return (
      <div ref={node => this.node = node} id="newsDetails">
        <SubComponentHeader/>
        <div className="newsDetailContent">
          <div style={{width: "1024px", margin: "0 auto"}}>
            <h1>{newsSingleData["headline"]}</h1>
            <h5>{newsSingleData["author"]}</h5>
            <h2>{newsSingleData["publishDate"]}</h2>
            <img className="newsimg" src={this.state.newsSingleImg} alt="newsimg"/>
            <div>{Parser(this.state.newsSingleContent)}</div>
          </div>
        </div>
        <div className="relatedArticles">
          <NewsComponent items={this.state.recommendedNews}
                         current={1}
                         itemscount={3}
                         fontColor={{color: "#000"}}/>
        </div>
        <SubComponentFooter />
      </div>
    )
  }
}

export default NewsDetail;
